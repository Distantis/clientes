<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Nueva Empresa</title>

    <?php include("css.php");?>

    <link rel="stylesheet" type="text/css" href="css/select2.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        

        <?php include("menu_izquierdo.php");?>

        <!-- top navigation -->
        <?php include("menu_superior_derecho.php");?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3></h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Cotizaciones</h2>
                    <ul class="nav navbar-right panel_toolbox  pull-rigth">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      
                  <br>
                    <form class="form-horizontal form-label-left input_mask">

                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left form_cot" name="cot" id="n_cot" placeholder="N° Cot">
                        <span class="fa fa-file-o form-control-feedback left" aria-hidden="true"></span>
                      </div>

                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left form_cot" name="file" id="n_file" placeholder="N° File">
                        <span class="fa fa-file-o form-control-feedback left" aria-hidden="true"></span>
                      </div>

                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left form_cot" name="pasajero" id="pax" placeholder="Pasajero(a)">
                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                        <br>
                      </div>

                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left fechas desde form_cot"  name="desde" id="desde" placeholder="Fecha Desde">
                        <span class="fa fa-calendar form-control-feedback left" aria-hidden="true"></span>
                      </div>

                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left fechas hasta form_cot" name="hasta" id="hasta" placeholder="Fecha Hasta">
                        <span class="fa fa-calendar form-control-feedback left" aria-hidden="true"></span>
                      </div>

                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                        <div class="icheckbox_flat-green">
                          <input type="checkbox" class="has-feedback-left flat" id="activar_fechas" checked="">
                        </div>
                        <small>Activar/desactivar Fechas</small>
                        <br>
                      </div>

                      <div class="col-md-12 col-sm-12 col-xs-12">
                      <br><br>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="form-group row">
                            <label for="cereador" class="col-md-3 col-sm-3 col-xs-12 col-form-label">Creador</label>
                              <i class="fa fa-spinner fa-spin" id="spinner-creador"></i>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <select name="creador" class="form-control select2 form_cot" id="creador">
                                </select>
                              </div>
                            </div>
                         </div>

                         <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="form-group row">
                            <label for="estado" class="col-md-3 col-sm-3 col-xs-12 col-form-label">Estado</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <select name="estado" class="form-control select2 form_cot" id="estado">
                                     <option></option>
                                     <option value="0">Confirmadas</option>
                                     <option value="1">Anuladas</option>
                                </select>
                              </div>
                          </div>
                        </div>
                      </div>


                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="form-group row">
                            <label for="destino" class="col-md-3 col-sm-3 col-xs-12 col-form-label">Destino</label>
                              <i class="fa fa-spinner fa-spin" id="spinner-destino"></i>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <select name="destino" class="form-control select2 form_cot" id="destino">
                                </select>
                              </div>
                            </div>
                         </div>

                         <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="form-group row">
                            <label for="hotel" class="col-md-3 col-sm-3 col-xs-12 col-form-label">Hotel</label>
                              <i class="fa fa-spinner fa-spin" id="spinner-hotel"></i>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <select name="hotel" class="form-control select2 form_cot" id="hoteles">
                                </select>
                              </div>
                          </div>
                        </div>
                      </div>


                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="form-group row">
                            <label for="operador" class="col-md-3 col-sm3 col-xs-12 col-form-label">Operador</label>
                              <i class="fa fa-spinner fa-spin" id="spinner-operador"></i>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <select name="operador" class="form-control select2 form_cot" id="operador">
                                </select>
                              </div>
                            </div>
                         </div>

                         <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="form-group row">
                            <label for="cliente" class="col-md-3 col-sm-3 col-xs-12 col-form-label">Cliente</label>
                              <i class="fa fa-spinner fa-spin" id="spinner-cliente"></i>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <select name="cliente" class="form-control select2 form_cot" id="cliente">
                                </select>
                              </div>
                          </div>
                        </div>
                      </div>


                      
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <hr>
                          <button class="btn btn-success btn-round pull-right" id="buscar">Buscar</button>
                        </div>
                      </div>

                    </form>
                    <br><br>
                    <hr>
                  </div>



                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div id="cotizaciones"></div>
                  </div>


                      
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <input type="hidden" class="nombre_cliente">
        <!-- /page content -->

        <!-- footer content -->
        <?php include("footer.php");?>
        <!-- /footer content -->
      </div>
    </div>

    
    <?php include("jquery.php");?>

    <script type="text/javascript" src="js/select2.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>


    <script>

    $(document).on("ready",function(){
        
        $(".select2").attr("disabled","disabled");
        $(".select2").select2({
            language: "es",
            placeholder: "Seleccionar..",
            allowClear: true
        });


        $('#desde').daterangepicker({
          singleDatePicker: true,
          singleClasses: "desde",
          locale: {
            format: 'DD-MM-YYYY'
          },
          language: 'es'

        });

        $('#hasta').daterangepicker({
          singleDatePicker: true,
          singleClasses: "hasta",
          locale: {
            format: 'DD-MM-YYYY'
          },
          language: 'es'
        });

        $(".fechas").attr("disabled","disabled").val("");


        $.post("sitio/Hotel.php",{tipo : 1},
                 function (data) {

                        try{
                         data = $.parseJSON(data);
                       }catch(err){
                         mostrar_notificacion("ERROR", "Esta ocurriendo un error en el sistema.", "error","stack-bottomleft");
                         return false;
                       }
                        var hotel = '<option></option>';
                        
                        $.each(data.hoteles, function (i, datos) {
                          hotel += '<option  value="' + datos.pk + '">' + datos.nombre_hotel + '</option>';
                        });
                       
                        $("#hoteles").html(hotel).removeAttr("disabled","disabled");
                        $("#estado").removeAttr("disabled","disabled");
                        $("#spinner-hotel").removeClass("fa-spinner fa-spin");


                 }
        );

        $.post("sitio/Hotel.php",{tipo : 2},
                 function (data) {

                        try{
                         data = $.parseJSON(data);
                       }catch(err){
                         mostrar_notificacion("ERROR", "Esta ocurriendo un error en el sistema.", "error","stack-bottomleft");
                         return false;
                       }
                        var ciudad = '<option></option>';
                        
                        $.each(data.ciudades, function (i, datos) {
                          if(datos.nombre_ciudad != "")
                            ciudad += '<option  value="' + datos.id_ciudad + '">' + datos.nombre_ciudad + '</option>';
                        });
                       
                        $("#destino").html(ciudad).removeAttr("disabled","disabled");
                        $("#spinner-destino").removeClass("fa-spinner fa-spin");


                 }
        );


        $.post("sitio/Hotel.php",{tipo : 3},
                 function (data) {

                        try{
                         data = $.parseJSON(data);
                       }catch(err){
                         mostrar_notificacion("ERROR", "Esta ocurriendo un error en el sistema.", "error");
                         return false;
                       }
                        var operador = '<option></option>';
                        
                        $.each(data.operadores, function (i, datos) {
                          operador += '<option  value="' + datos.id_operador + '">' + datos.nombre_hot + '</option>';
                        });
                       
                        $("#operador").html(operador).removeAttr("disabled","disabled");
                        $("#spinner-operador").removeClass("fa-spinner fa-spin");


                 }
        );


         $.post("sitio/Hotel.php",{tipo : 4},
                 function (data) {

                        try{
                         data = $.parseJSON(data);
                       }catch(err){
                         mostrar_notificacion("ERROR", "Esta ocurriendo un error en el sistema.", "error");
                         return false;
                       }
                        var cliente = '<option></option>';
                        
                        $.each(data.clientes, function (i, datos) {
                          cliente += '<option  value="' + datos.id_opcts + '">' + datos.id_opcts + '</option>';
                        });
                       
                        $("#cliente").html(cliente).removeAttr("disabled","disabled");
                        $("#spinner-cliente").removeClass("fa-spinner fa-spin");


                 }
        );


        // Usuarios


        $.post("sitio/Usuario.php",{tipo : 1},
                 function (data) {

                        try{
                         data = $.parseJSON(data);
                       }catch(err){
                         mostrar_notificacion("ERROR", "Esta ocurriendo un error en el sistema.", "error","stack-bottomleft");
                         return false;
                       }
                        var creador = '<option></option>';
                        
                        $.each(data.creador, function (i, datos) {
                          if(datos.usu_login != "")
                            creador += '<option  value="' + datos.id_usuario + '">' + datos.usu_login + '</option>';
                        });
                       
                        $("#creador").html(creador).removeAttr("disabled","disabled");
                        $("#spinner-creador").removeClass("fa-spinner fa-spin");



                 }
        );



    });


$('select').on('select2:open', function() {
    $('.select2-search--dropdown .select2-search__field').attr('placeholder', 'Buscar...');
});


$("#buscar").on("click", function(e){
    e.preventDefault();
    $(this).html("<i class='fa fa-spinner fa-spin'></i>").addClass("disabled");
    $("#cotizaciones").html("");

    var cot = $("#n_cot").val();
    var file = $("#n_file").val();
    var desde = $("#desde").val();
    var hasta = $("#hasta").val();
    var estado = $("#estado").val();
    var hotel = $("#hoteles").val();
    var pax = $("#pax").val();
    var ciudad = $("#destino").val();

    var todos = [];
    var arreglo_select = [];

    $(".form_cot").each(function(){
      var data = {};
      var nombre = $(this).attr("name");
      var valor = $(this).val();

      if(valor != ""){
        data[nombre] = valor;
        todos.push(data);
      }

    });

    var cont = 0;
    $(".select2").each(function(){
      var valor = $(this).val();

      if(valor != "")
        cont+=1;

    });

    if(todos.length == 0){
      mostrar_notificacion("Advertencia","No dejar campos en blanco.","","stack-bottomleft");
      $("#buscar").html("Buscar").removeClass("disabled");
      return false;
    }

    var cont2 = 0;

    if(cot != "" || file != "" || pax != "")
      cont2+=1;


    if(cont == 1 && (desde == "" || hasta == "") && cont2 == 0){
      mostrar_notificacion("Advertencia","Si esta eligiendo un estado sin file o cot, debe ingresar fecha desde y hasta para que no se demore tanto la respuesta.","","stack-bottomleft");
      $("#cotizaciones").html("");
      $("#buscar").html("Buscar").removeClass("disabled");
      
    }else{
        //console.log("paso");
        //return false;
        $.post("sitio/Data.php",{tipo : 1,desde:desde,hasta:hasta,estado:estado,hotel:hotel,cot:cot,file:file,pax:pax,ciudad:ciudad},
                 function (data) {

                        try{
                         data = $.parseJSON(data);
                       }catch(err){
                         mostrar_notificacion("ERROR", "Esta ocurriendo un error en el sistema.", "error","stack-bottomleft");
                         return false;
                       }


                       var tabla ='<table class="table table-bordered table-striped" id="tabla_cotizaciones" width="100%"><thead>'
                              +'<tr>'
                                  +'<td align="center"><strong>Cot</strong></td>'
                                  +'<td align="center"><strong>TMA</strong></td>'
                                  +'<td align="center"><strong>Estado</strong></td>'
                                  +'<td align="center" style="white-space: nowrap !important;"><strong>Creación</strong></td>'
                                  +'<td align="center" style="white-space: nowrap !important;"><strong>Fec Desde</strong></td>'
                                  +'<td align="center" style="white-space: nowrap !important;"><strong>Fec Hasta</strong></td>'
                                  +'<td align="center"><strong>Operador</strong></td>'
                                  +'<td align="center"><strong>Nemo</strong></td>'
                                  +'<td align="center"><strong>Creador</strong></td>'
                                  +'<td align="center"><strong>Valor</strong></td>'
                                  +'<td align="center"><strong>Hotel</strong></td>'
                                  +'<td align="center" style="white-space: nowrap !important;"><strong>N° Confirm</strong></td>'
                                  +'<td align="center"><strong>Pax</strong></td>'
                                  +'<td align="center"><strong>Or</strong></td>'
                              +'</tr></thead><tbody>';

                        
                        $.each(data.cotizaciones, function (i, datos) {

                          var estado_reserva = "";
                          var or_reserva = "";

                          if(datos.estado == 0)
                               estado_reserva = "<td align='center' class='bg-success'><small class='tex-success'><strong>CONFIRMADO</strong></small></td>";
                          else
                               estado_reserva = "<td align='center' class='bg-danger'><small class='tex-danger'><strong>ANULADA</strong></small></td>";

                          if(datos.or == 7)
                               or_reserva = "<td align='center'><small>NO</small></td>";
                          else
                               or_reserva = "<td align='center'><small>SI</small></td>";
                          
                          tabla += "<tr>"
                                      +"<td align='center'><small>"+datos.id_cot+"</small></td>"
                                      +"<td align='center'><small>"+datos.tma+"</small></td>"
                                      +estado_reserva
                                      +"<td align='center'><small>"+datos.fecha_cot+"</small></td>"
                                      +"<td align='center'><small>"+datos.desde+"</small></td>"
                                      +"<td align='center'><small>"+datos.hasta+"</small></td>"
                                      +"<td align='center'><small>"+datos.operador+"</small></td>"
                                      +"<td align='center'><small>"+datos.nemo+"</small></td>"
                                      +"<td align='center'><small>"+datos.usuario_creador+"</small></td>"
                                      +"<td align='center'><small>"+datos.valor+"</small></td>"
                                      +"<td align='center'><small>"+datos.hotel+"</small></td>"
                                      +"<td align='center'><small>"+datos.nmr_confirmacion+"</small></td>"
                                      +"<td align='center'><small>"+datos.pax+"</small></td>"
                                      +or_reserva
                                  +"</tr>"   
                        });


                        if(data.cotizaciones === undefined || data.cotizaciones == "" || data.cotizaciones === null)
                          tabla = '<div class="alert alert-danger alert-dismissible fade in" role="alert">No existen reservas para el filtro ingresado.</div>';
                       
                        $("#cotizaciones").html(tabla);

                 }
        ).done(function(){
            $("#buscar").html("Buscar").removeClass("disabled");

            $("#tabla_cotizaciones").DataTable({
              "scrollY":        "400px",
              "scrollX" : "100%",
              "scrollCollapse": true,
                    "aaSorting": [[0, 'asc']],
                    "oLanguage": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ ",
                        "sZeroRecords": "No se han encontrado datos disponibles",
                        "sEmptyTable": "No se han encontrado datos disponibles",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando  del 0 al 0 de un total de 0 ",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ datos)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar: ",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Última",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    }

             });


        });



    }



});


    $(document).on('ifUnchecked','#activar_fechas', function () { 
        $(".fechas").removeAttr("disabled","disabled");
    });

    $(document).on('ifChecked','#activar_fechas', function () { 
        $(".fechas").attr("disabled","disabled").val("");
    });
    

    </script>


  </body>
</html>
