<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Nueva Empresa</title>

    <?php include("css.php");?>

    <link rel="stylesheet" type="text/css" href="css/select2.css">

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        

        <?php include("menu_izquierdo.php");?>

        <!-- top navigation -->
        <?php include("menu_superior_derecho.php");?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">

              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Servicio Individual de Hotelería</h2>
                    <ul class="nav navbar-right panel_toolbox  pull-rigth">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      
                    
                    <br>
                    <form class="form-horizontal form-label-left input_mask">

                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left fechas desde form_servicio_individual" id="desde" name="desde" placeholder="Fecha Desde">
                        <span class="fa fa-calendar form-control-feedback left" aria-hidden="true"></span>
                      </div>

                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left fechas hasta form_servicio_individual" id="hasta" name="hasta" placeholder="Fecha Hasta">
                        <span class="fa fa-calendar form-control-feedback left" aria-hidden="true"></span>
                      </div>

                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <br><br>
                      </div>


                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="form-group row">
                            <label for="destino" class="col-md-3 col-sm-3 col-xs-12 col-form-label">Destino</label>
                              <i class="fa fa-spinner fa-spin" id="spinner-destino"></i>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <select name="destino" class="form-control select2 form_servicio_individual" id="destino">
                                </select>
                              </div>
                            </div>
                         </div>

                         <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="form-group row">
                            <label for="tipo_hotel" class="col-md-3 col-sm-3 col-xs-12 col-form-label">Tipo Hotel</label>
                              <i class="fa fa-spinner fa-spin" id="spinner-tipo_hotel"></i>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <select name="tipo_hotel" class="form-control select2 form_servicio_individual" id="tipo_hotel">
                                    <option></option>
                                    <option value="3">5*</option>
                                    <option value="2">4*</option>
                                    <option value="1">3*</option>
                                    <option value="26">2*</option>
                                    <option value="27">BOUTIQUE</option>
                                </select>
                              </div>
                          </div>
                        </div>
                      </div>
                      

                      <div class="col-md-12 col-sm-12 col-xs-12">
                          
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group row">
                              <label for="operador" class="col-md-3 col-sm-3 col-xs-12 col-form-label">Seleccione tipo</label>
                                <i class="fa fa-spinner fa-spin" id="spinner-operador"></i>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <select name="operador" class="form-control select2 form_servicio_individual" id="operador">
                                  </select>
                                </div>
                              </div>
                           </div>

                           <div class="col-md-6 col-sm-6 col-xs-12 nemo">
                            <div class="form-group row">
                              <label for="nemo" class="col-md-3 col-sm-3 col-xs-12 col-form-label">Nemo</label>
                                <i class="fa fa-spinner fa-spin" id="spinner-nemo"></i>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <select name="nemo" class="form-control select2 form_servicio_individual" id="nemo">
                                      <option></option>
                                  </select>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback nemo">
                                <i class="fa fa-info-circle text-warning " id="add_nemo"></i>
                              <small>Agregar Nemo</small>
                            </div>
                            <br>
                              <hr>
                            <br>
                          </div>



                        </div>


                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group row">
                              <label for="pasajero" class="col-md-3 col-sm-3 col-xs-12 col-form-label">N° Pasajeros</label>
                                <i class="fa fa-spinner fa-spin" id="spinner-pasajero"></i>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <select name="pasajero" class="form-control select2 form_servicio_individual" id="pasajero">
                                      <option></option>
                                      <option value="1">1</option>
                                      <option value="2">2</option>
                                      <option value="3">3</option>
                                      <option value="4">4</option>
                                      <option value="5">5</option>
                                      <option value="6">6</option>
                                      <option value="7">7</option>
                                      <option value="8">8</option>
                                      <option value="9">9</option>
                                  </select>
                                </div>
                              </div>
                           </div>

                           <div class="col-md-6 col-sm-6 col-xs-12 tarifa">
                            <div class="form-group row">
                              <label for="tarifa" class="col-md-3 col-sm-3 col-xs-12 col-form-label">Tarifa</label>
                                <i class="fa fa-spinner fa-spin" id="spinner-tarifa"></i>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <select name="tarifa" class="form-control select2 form_servicio_individual" id="tarifa">
                                      <option></option>
                                  </select>
                                </div>
                            </div>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                              <hr><br>
                          </div>
                      </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="panel panel-default">
                             <div class="panel-heading"><strong>TIPO HABITACION</strong></div>
                              <div class="panel-body">
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group row">
                                          <label for="sgl" class="col-md-3 col-sm-3 col-xs-12 col-form-label">Single</label>
                                            <i class="fa fa-spinner fa-spin" id="spinner-sgl"></i>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                              <select name="sgl" class="form-control select2 form_servicio_individual" id="sgl">
                                                  <option></option>
                                                  <option value="1">1</option>
                                                  <option value="2">2</option>
                                                  <option value="3">3</option>
                                                  <option value="4">4</option>
                                                  <option value="5">5</option>
                                                  <option value="6">6</option>
                                                  <option value="7">7</option>
                                                  <option value="8">8</option>
                                                  <option value="9">9</option>
                                              </select>
                                            </div>
                                          </div>
                                       </div>

                                       <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group row">
                                          <label for="twn" class="col-md-3 col-sm-3 col-xs-12 col-form-label">Doble Twin</label>
                                            <i class="fa fa-spinner fa-spin" id="spinner-twn"></i>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                              <select name="twn" class="form-control select2 form_servicio_individual" id="twn">
                                                  <option></option>
                                                  <option value="1">1</option>
                                                  <option value="2">2</option>
                                                  <option value="3">3</option>
                                                  <option value="4">4</option>
                                                  <option value="5">5</option>
                                                  <option value="6">6</option>
                                                  <option value="7">7</option>
                                                  <option value="8">8</option>
                                                  <option value="9">9</option>
                                              </select>
                                            </div>
                                          </div>
                                      </div>
                                  </div>


                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group row">
                                          <label for="dbl" class="col-md-3 col-sm-3 col-xs-12 col-form-label">Doble Matrimonial</label>
                                            <i class="fa fa-spinner fa-spin" id="spinner-dbl"></i>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                              <select name="dbl" class="form-control select2 form_servicio_individual" id="dbl">
                                                  <option></option>
                                                  <option value="1">1</option>
                                                  <option value="2">2</option>
                                                  <option value="3">3</option>
                                                  <option value="4">4</option>
                                                  <option value="5">5</option>
                                                  <option value="6">6</option>
                                                  <option value="7">7</option>
                                                  <option value="8">8</option>
                                                  <option value="9">9</option>
                                              </select>
                                            </div>
                                          </div>
                                       </div>

                                       <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group row">
                                          <label for="tpl" class="col-md-3 col-sm-3 col-xs-12 col-form-label">Triple</label>
                                            <i class="fa fa-spinner fa-spin" id="spinner-tpl"></i>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                              <select name="tpl" class="form-control select2 form_servicio_individual" id="tpl">
                                                  <option></option>
                                                  <option value="1">1</option>
                                                  <option value="2">2</option>
                                                  <option value="3">3</option>
                                                  <option value="4">4</option>
                                                  <option value="5">5</option>
                                                  <option value="6">6</option>
                                                  <option value="7">7</option>
                                                  <option value="8">8</option>
                                                  <option value="9">9</option>
                                              </select>
                                            </div>
                                          </div>
                                      </div>
                                  </div>

                              </div>
                        </div>
                      </div>



                      
                        



                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <hr>
                          <button class="btn btn-round btn-success pull-right" id="buscar">Buscar</button>
                          <br><br><br>
                        </div>
                      </div>

                    </form>


                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="panel panel-success confirmaciones" style="display:none;">
                             <div class="panel-heading"><strong>CONFIRMACIONES</strong></div>
                              <div class="panel-body">
                                  <div id="confirmaciones"></div>
                              </div>
                        </div>
                    </div>

                     <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="panel panel-warning on_request" style="display:none;">
                             <div class="panel-heading"><strong>ONREQUEST</strong></div>
                              <div class="panel-body">
                                  <div id="onrequest"></div>
                              </div>
                        </div>
                    </div>
                      
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>






        <!--- MODALES -->


        <div id="modal_nemo" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><center><i class="fa fa-warning text-warning"></i> Agregar Nemo</center></h4>
                </div>
                <div class="modal-body">

                  <div class="row">
                          <div class="col-md-12">
                              <div class="form-group">

                                <div class="col-md-12 ">
                                      <table class="table table-bordered table-striped">
                                        <tbody>
                                          <tr>
                                            <th>Nemo</th>
                                            <th>Razon social</th>
                                          </tr>
                                          <tr>
                                            <td><input type="text" name="nom_nemo" id="nom_nemo" class="form-control"></td>
                                            <td><input type="text" name="razon" id="razon" class="form-control"></td>
                                          </tr>

                                          <tr>
                                            <th>Holding</th>
                                            <th>Grupo Holding</th>
                                          </tr>
                                          <tr>
                                            <td><input type="text" name="holding" id="holding" class="form-control"></td>
                                            <td><input type="text" name="grupo_holding" id="holding" class="form-control"></td>
                                          </tr>

                                        </tbody>
                                      </table>

                                      <button id="agregar_nemo" class="btn btn-success pull-left">AGREGAR</button>

                                  </div>
                              </div>
                          </div>
                      </div>
                  <footer><button class="btn btn-warning pull-right" class="close" data-dismiss="modal">Cerrar</button><br></footer>
              </div>
            </div>
          </div>
        </div>






        <input type="hidden" class="nombre_cliente">
        <!-- /page content -->

        <!-- footer content -->
        <?php include("footer.php");?>
        <!-- /footer content -->
      </div>
    </div>

    
    <?php include("jquery.php");?>


     <script type="text/javascript" src="js/select2.js"></script>


    <script>

        $(document).on("ready",function(){
            
            $(".select2").attr("disabled","disabled");
            $(".select2").select2({
                language: "es",
                placeholder: "Seleccionar..",
                allowClear: true
            });



            $('#desde').daterangepicker({
              singleDatePicker: true,
              singleClasses: "desde",
              locale: {
                format: 'DD-MM-YYYY'
              },
              language: 'es'

            });

            $('#hasta').daterangepicker({
              singleDatePicker: true,
              singleClasses: "hasta",
              locale: {
                format: 'DD-MM-YYYY'
              },
              language: 'es'
            });

            $(".fechas").val("");

            $.post("sitio/Hotel.php",{tipo : 2},
                 function (data) {

                        try{
                         data = $.parseJSON(data);
                       }catch(err){
                         mostrar_notificacion("ERROR", "Esta ocurriendo un error en el sistema.", "error","stack-bottomleft");
                         return false;
                       }
                        var ciudad = '<option></option>';
                        
                        $.each(data.ciudades, function (i, datos) {
                          if(datos.nombre_ciudad != "")
                            ciudad += '<option  value="' + datos.id_ciudad + '">' + datos.nombre_ciudad + '</option>';
                        });
                       
                        $("#destino").html(ciudad).removeAttr("disabled","disabled");
                        $("#spinner-destino").removeClass("fa-spinner fa-spin");

                        $("#tipo_hotel,#pasajero,#sgl,#twn,#dbl,#tpl").removeAttr("disabled","disabled");
                        $("#spinner-tipo_hotel,"
                                +"#spinner-pasajero,"
                                +"#spinner-sgl,"
                                +"#spinner-twn,"
                                +"#spinner-dbl,"
                                +"#spinner-tpl"
                                ).removeClass("fa-spinner fa-spin");


                 }
            ).done(function(){
              agregar_tipotarifa();
            });


            $.post("sitio/Hotel.php",{tipo : 3},
                 function (data) {

                        try{
                         data = $.parseJSON(data);
                       }catch(err){
                         mostrar_notificacion("ERROR", "Esta ocurriendo un error en el sistema.", "error","stack-bottomleft");
                         return false;
                       }
                        var operadores = '<option></option>';
                        
                        $.each(data.operadores, function (i, datos) {
                          if(datos.nombre_hot != "")
                            operadores += '<option  value="' + datos.id_operador + '">' + datos.nombre_hot + '</option>';
                        });
                       
                        $("#operador").html(operadores).removeAttr("disabled","disabled");
                        $("#spinner-operador").removeClass("fa-spinner fa-spin");

                 }
            );

            $.post("sitio/Hotel.php",{tipo : 5},
                 function (data) {

                        try{
                         data = $.parseJSON(data);
                       }catch(err){
                         mostrar_notificacion("ERROR", "Esta ocurriendo un error en el sistema.", "error","stack-bottomleft");
                         return false;
                       }
                        var nemo = '<option></option>';
                        
                        $.each(data.nemo, function (i, datos) {
                          if(datos.nombre_hot != "")
                            nemo += '<option  value="' + datos.id_nemo + '">' + datos.nemo + ' (' + datos.r_social + ')</option>';
                        });
                       
                        $("#nemo").html(nemo).removeAttr("disabled","disabled");
                        $("#spinner-nemo").removeClass("fa-spinner fa-spin");

                 }
            );



            $('select').on('select2:open', function() {
                $('.select2-search--dropdown .select2-search__field').attr('placeholder', 'Buscar...');
            });
            

        });


        $("#add_nemo").on("click", function(){
          $("#modal_nemo").modal("show");
        });




        $("#buscar").on("click", function(e){

            e.preventDefault();
            $("#confirmaciones").html("");
            $("#onrequest").html("");

            $(".confirmaciones").css("display","none");
            $(".on_request").css("display","none");

            $(this).html("<i class='fa fa-spinner fa-spin'></i>").addClass("disabled");

            var todo = [];

            $(".form_servicio_individual").each(function(){

                var obj = {};

                var nombre = $(this).attr("name");
                var valor = $(this).val();

                switch(nombre){
                  case "sgl":
                  case "twn":
                  case "dbl":
                  case "tpl":
                    if(valor == "")
                        valor = 0;
                  break;
                }

                  obj[nombre] = valor;
                  todo.push(obj);

            });

            var cliente =  $(".nombre_cliente").val();

             $.post("sitio/Cliente.php",{tipo : 1, servicio : todo, cliente : cliente},
                 function (data) {
                      try{
                       data = $.parseJSON(data);
                     }catch(err){
                       mostrar_notificacion("ERROR", "Esta ocurriendo un error en el sistema.", "error","stack-bottomleft");
                       $(".confirmaciones").css("display","none");
                       $(".on_request").css("display","none");
                       return false;
                     }


                     var tabla_conf ='<table class="table table-bordered table-responsive" id="tabla_confirmaciones" width="100%"><thead>'
                              +'<tr>'
                                  +'<td align="center"><strong>Hotel</strong></td>'
                                  +'<td align="center"><strong>Categoría</strong></td>'
                                  +'<td align="center"><strong>Cat</strong></td>'
                                  +'<td align="center"><strong>Días</strong></td>'
                                  +'<td align="center"><strong>Tarifas</strong></td>'
                                  +'<td align="center"><strong>Valor total</strong></td>'
                                  +'<td align="center"><strong>Reservar</strong></td>'
                              +'</tr></thead><tbody>';

                    var tabla_onrequest ='<table class="table table-bordered table-responsive" id="tabla_onrequest" width="100%"><thead>'
                              +'<tr>'
                                  +'<td align="center"><strong>Hotel</strong></td>'
                                  +'<td align="center"><strong>Categoría</strong></td>'
                                  +'<td align="center"><strong>Cat</strong></td>'
                                  +'<td align="center"><strong>Días</strong></td>'
                                  +'<td align="center"><strong>Tarifas</strong></td>'
                                  +'<td align="center"><strong>Valor total</strong></td>'
                                  +'<td align="center"><strong>Reservar</strong></td>'
                              +'</tr></thead><tbody>';

                     $.each(data.confirmacion_instantanea, function (i, datos) {

                      var total = parseFloat(datos.valor_sgl)+parseFloat(datos.valor_dbl)+parseFloat(datos.valor_tpl);
                          
                          tabla_conf += "<tr>"
                                      +"<td align='left'><strong>"+datos.nombre_hotel.toUpperCase()+"</strong></td>"
                                      +"<td align='left'><span class='badge badge-danger'>"+datos.nombre_hab.toUpperCase()+"</span></td>"
                                      +"<td align='center'></td>"
                                      +"<td align='center'></td>"
                                      +"<td align='center'></td>"
                                      +"<td align='center'>"+total+"</td>"
                                      +"<td align='center'><input type='radio' name='reservar'></td>"
                                  +"</tr>"   



                     });

                     $("#confirmaciones").html(tabla_conf);


                      $.each(data.on_request, function (i, datos) {
                          
                          var total = parseFloat(datos.valor_sgl)+parseFloat(datos.valor_dbl)+parseFloat(datos.valor_tpl);
                          
                          tabla_onrequest += "<tr>"
                                      +"<td align='left'><strong>"+datos.nombre_hotel.toUpperCase()+"</strong></td>"
                                      +"<td align='left'><span class='badge badge-danger'>"+datos.nombre_hab.toUpperCase()+"</span></td>"
                                      +"<td align='center'></td>"
                                      +"<td align='center'></td>"
                                      +"<td align='center'></td>"
                                      +"<td align='center'>"+total+"</td>"
                                      +"<td align='center'><input type='radio' name='reservar'></td>"
                                  +"</tr>"   
                     });

                     $("#onrequest").html(tabla_onrequest);
                 }


            ).done(function(){
              $("#buscar").html("Buscar").removeClass("disabled");
              $(".confirmaciones").css("display","block");
              $(".on_request").css("display","block");
            });

        });




        function agregar_tipotarifa(){

            var nombre_cliente = $(".nombre_cliente").val();

            switch(nombre_cliente){

              case "cocha":
                  tarifa = '<option  value="1">Corporativa</option><option  value="2">Vacacional</option>';
                  $("#tarifa").append(tarifa).removeAttr("disabled","disabled");
                  $("#spinner-tarifa").removeClass("fa-spinner fa-spin");
              break;

              case "turavion":
                  //$("#tarifa").html("").removeAttr("disabled","disabled");
                  //$("#spinner-tarifa").removeClass("fa-spinner fa-spin");
                  $(".nemo").remove();
                  $(".tarifa").remove();
              break;

            }


        }

    </script>

  </body>
</html>
