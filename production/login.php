<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gentelella Alela! | </title>

    <!-- Bootstrap -->
    <link href="../clientes/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../clientes/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../clientes/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="../clientes/vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../clientes/build/css/custom.min.css" rel="stylesheet">

    <link href="../h2o/hoteleria_2/prueba/css/jquery.gritter.css" rel="stylesheet">
    <link href="../h2o/hoteleria_2/prueba/css/style_gritter.css"  rel="stylesheet">


  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <input type="hidden" id="cliente" value="<?=$subdominio?>">

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
          <center><img src='../<?=$subdominio?>/images/<?=$subdominio?>.png' style='width:300px;height:120px;'></center>
            <form>
              <h1>-</h1>
              <div>
                <input type="text" class="form-control" placeholder="Usuario" id="usuario" required="" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" id="pass" required="" />
              </div>
              <div>
                <button class="btn btn-default" id="login">Iniciar Sesión</a>
                <!--a class="reset_pass" href="#">Lost your password?</a-->
              </div>

              <div class="clearfix"></div>

              <div class="separator">

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1>Nueva Empresa</h1>
                  <p>©2018 Todos los derechos reservados a la empresa Nueva Empresa.</p>
                </div>
              </div>
            </form>
          </section>
        </div>

      </div>
    </div>


    <script src="../clientes/vendors/jquery/dist/jquery.min.js"></script>
    <script src="../h2o/hoteleria_2/prueba/js/jquery.gritter.js" type="text/javascript"></script>
    <script src="../h2o/hoteleria_2/prueba/js/notificaciones.js" type="text/javascript"></script>

    <script>

    $("#login").on("click", function(e){

       e.preventDefault();
        
       var usuario = $("#usuario").val();
       var pass = $("#pass").val();
       var cliente = $("#cliente").val();

       if(usuario == "" || pass == ""){
          mostrar_notificacion("Advertencia", "No dejar campos en blanco", "warning", "bottom-left");
          return false;
       }

       $.post("/clientes/production/sitio/login.php",{tipo : 2, usuario:usuario, pass:pass, cliente : cliente},
                 function (data) {

               data = $.parseJSON(data);

              if(data.respuesta != 0)
                window.location.href = "/clientes/production/plain_page.php";
              else 
                mostrar_notificacion("Advertencia", "El usuario no existe", "warning", "bottom-left");
       

       });

     });


    </script>


  </body>
</html>
