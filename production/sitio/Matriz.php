<?php 
	
	/**
	* 
	*/
	class Matriz 
	{

		//protected $sql_con;
		protected $sessiones = array();
		protected $datos = array();
		protected $info = array();
		
		function __construct($db1,$cliente)
		{	
			error_reporting(0);
			session_start();
			$this->sql_con = $db1;			
			$this->info["cliente"] = $cliente;
			$this->buscar_bd();
		}


		public function Disponibilidad($reserva){

			$this->info["reserva"] = $reserva;

			// la quite de la consulta  AND hot.hot_barco = 0 hay que ver que es cocha

			$this->info["hotdet"] = array();

			$select_por_cliente = "";
			$atributos_por_cliente = "";

			switch ($this->info["cliente"]) {
				case 'cocha':
					   $atributos_por_cliente = " AND (id_tipotarifa IN (2, 142, 143, 144, 27, 168, 169) 
							      				  AND id_tipotarifa2 IN (27) )
							      				";
				break;
				
				case "veci":
						$atributos_por_cliente = "AND id_tipotarifa IN (2, 3, 9, 139) AND hd.visible_for IN (3030) ";
				break;

				case "turavion":

					  $select_por_cliente = "
					  							hd.hd_sgl_vta,
					  							hd.hd_dbl_vta,
					  							hd.hd_tpl_vta,
					  						";

					  $atributos_por_cliente = "AND (
											      id_tipotarifa IN (2, 3, 9, 43, 70, 149, 146, 159, 197, 350, 359, 147) 
											      OR hd.por_h2o = 1
											    ) 
											    AND cod_cliente IN (0, 1) ";
				break;

				case "travelclub":

					$atributos_por_cliente = "

												AND (despromo IS NULL 
                                                      OR despromo = '') 

                                                AND (
												      id_tipotarifa IN (2, 142, 143, 144, 27) 
												      OR id_tipotarifa2 IN (27, 2)
												    )       
											";
				break;

				case "carlson":

						$opp = "";
						foreach ($this->info["reserva"] as $key => $value) {
							$opp = $value["operador"];
						}
						
						$atributos_por_cliente = "

												AND hd.id_cliente IN (".$opp.", 0)
												AND hd.id_tipotarifa IN (2,3,4,9,16,17,30,31,32,43,49,139,161,162,163,164,165,166,168)	
										";
				break;
			}



			//foreach ($this->info["reserva"] as $key => $value) {

					$consulta = "SELECT 
									hd.id_hotdet,
									hot.hot_nombre,
									th.id_tipohab,
									hot.id_hotel,
									hd_sgl,
									hd_dbl,
									hd_tpl,

										$select_por_cliente

									hd.id_tipohabitacion,
									th.id_tipohab,
									th.th_nombre
							  		FROM
							    ".$this->info["bd"].".hotdet hd
							    join ".$this->info["bd"].".stock s
							     on s.id_hotdet = hd.id_hotdet
							    join ".$this->info["bd"].".hotel hot 
							     on hot.id_hotel = hd.id_hotel
							    left join ".$this->info["bd"].".tipohabitacion th
							     on th.id_tipohabitacion = hd.id_tipohabitacion
							  WHERE hd.id_hotdet = s.id_hotdet 
							    AND s.sc_fecha >= '".$this->info["reserva"]["desde"]."' 
							    AND s.sc_fecha < '".$this->info["reserva"]["hasta"]."' 
							   
							    AND (th.id_tipohab = hd.id_tipohabitacion or th.id_tipohabitacion = hd.id_tipohabitacion)
							    AND hot.hot_estado = 0 
							    AND hot.hot_activo = 0 
							    AND hot.id_ciudad = ".$this->info["reserva"]["destino"]."
							    AND s.sc_cerrado = 0 
							    -- AND hd.id_area = ".$this->info["reserva"]["area"]."
							    -- and hd.hd_mon = ".$this->info["reserva"]["area"]."
							    AND (sc_tarifa = 0 
							      OR sc_tarifa = 2) 
							    AND (sc_cerrado_sgl = 0) 
							    AND (habxvender >= 1 
							      OR habxvender = 0) 
							    AND hot.id_hotel = hd.id_hotel 
							    AND hd.id_tipohabitacion IS NOT NULL 
							    
							    $atributos_por_cliente
					
							    AND hd.hd_estado = 0 
							    AND s.sc_estado = 0 
								group by (s.id_hotdet)
							    ";
					//echo $consulta."<br>";		    
				    $traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

				    if($traer->RecordCount() > 0){

				    	while(!$traer->EOF){

					    	$hotdet = $traer->Fields("id_hotdet");
					    	$habitacion = $traer->Fields("id_tipohab");
					    	$hotel = $traer->Fields("id_hotel");
					    	$pk = $this->traer_pk($hotel);
					    	$sgl = $traer->Fields("hd_sgl");
					    	$dbl = $traer->Fields("hd_dbl");
					    	$tpl = $traer->Fields("hd_tpl");
					    	$hab_normal = $traer->Fields("id_tipohabitacion");
					    	$hab_global = $traer->Fields("id_tipohab");
					    	$nombre_hab = $traer->Fields("th_nombre");

					    	// Para mostrar valor sin markup ni nada

					    	$sgl_tarifa = round($traer->Fields("hd_sgl"));
					    	$dbl_tarifa = round($traer->Fields("hd_dbl"));
					    	$tpl_tarifa = round($traer->Fields("hd_tpl"));


					    	$nombre_hotel = utf8_encode($traer->Fields("hot_nombre"));
					    	
					    	$this->es_global($pk);


					    	if($this->info["reserva"]["sgl"] == 0) $sgl = 0;
			    			if($this->info["reserva"]["twn"] == 0 and $this->info["reserva"]["dbl"] == 0) $dbl = 0;
			    			if($this->info["reserva"]["tpl"] == 0) $tpl = 0;



					    	switch ($this->info["cliente"]) {
					    		case 'cocha':
					    			$this->buscar_mkup($this->info["reserva"]["operador"],$hotel);
					    			$sgl = round((($sgl)*1.19)/$this->info["markup"]);
					    			$dbl = round((($dbl*2)*1.19)/$this->info["markup"]);
					    			$tpl = round((($tpl*3)*1.19)/$this->info["markup"]);
					    		break;

					    		case 'veci':


                                    // hay que modificar los valores de las habitaciones para que envíe la información requerida real en total_neto y total_markup

					    			$this->buscar_mkup($this->info["reserva"]["operador"],$hotel);


					    			$total_neto = round($sgl+$dbl*2+$tpl*3);
					    			$total_markup = round($total_neto * ($this->info["markup"] - 1));
					    			$total_iva = round(($total_neto + $total_markup) * 0.19);
					    			$total = $total_neto + $total_markup + $total_iva;
					    			//echo $total_neto."<br>";
					    			//echo $total_markup."<br>";
					    			//echo  $total_iva."<br>";
					    			//echo  $total."<br>";
					    			$sgl = round((($sgl)*1.19)/$this->info["markup"]);
					    			$dbl = round((($dbl*2)*1.19)/$this->info["markup"]);
					    			$tpl = round((($tpl*3)*1.19)/$this->info["markup"]);
					    		break;


					    		case 'turavion':
					    		case 'travelclub':

					    		    $this->info["id_hotdet_markup"] = $hotdet;
					    		    $this->info["area"] = $this->info["reserva"]["area"];

					    		    switch ($this->info["reserva"]["area"]) {
					    		    	case 1:
					    		    		$sgl = $traer->fields("hd_sgl_vta");
					    		    		$dbl = $traer->fields("hd_dbl_vta");
					    		    		$tpl = $traer->fields("hd_tpl_vta");
					    		    	break;
					    		    }

					    			$this->buscar_mkup($this->info["reserva"]["operador"],$hotel);


					    			$total_neto_sgl = round(($sgl)/$this->info["markup"]);
					    			$total_neto_dbl = round(($dbl*2)/$this->info["markup"]);
					    			$total_neto_tpl = round(($tpl*3)/$this->info["markup"]);

					    			$total = round($total_neto_sgl+$total_neto_dbl+$total_neto_tpl);

					    			$sgl = round((($sgl)*1.19)/$this->info["markup"]);
					    			$dbl = round((($dbl*2)*1.19)/$this->info["markup"]);
					    			$tpl = round((($tpl*3)*1.19)/$this->info["markup"]);

					    			//echo $hotdet."<=>".$sgl_tarifa."<=>".$total_neto_sgl."<=>".$total."<=======>".$sgl."<br>";


					    		break;


					    		case "carlson":
					    		case "mundotour":

					    			$this->info["id_hotdet_markup"] = $hotdet;
					    		    $this->info["area"] = $this->info["reserva"]["area"];
					    		    $this->info["desde_markup"] = $this->info["reserva"]["desde"];
					    		    $this->info["hasta_markup"] = $this->info["reserva"]["hasta"];

					    		    $this->info["valor_sgl"] = $sgl;
					    		    $this->info["valor_dbl"] = $dbl;
					    		    $this->info["valor_tpl"] = $tpl;

					    		    echo $sgl."<=>".$dbl."<=>".$tpl."<br>";

					    		    $this->buscar_mkup($this->info["reserva"]["operador"],$hotel);

					    		    $sgl = round($this->info["valor_sgl"]);//round((($sgl))/$this->info["markup_sgl"]);
					    			$dbl = round($this->info["valor_dbl"]);//round((($dbl*2))/$this->info["markup_dbl"]);
					    			$tpl = round($this->info["valor_tpl"]);//round((($tpl*3))/$this->info["markup_tpl"]);

					    		break;

					    	}



						    	$datos = array(
						    					"hotdet"=>$hotdet,
						    					"sgl"=>$this->info["reserva"]["sgl"],
						    					"twn"=>$this->info["reserva"]["twn"],
						    					"dbl"=>$this->info["reserva"]["dbl"],
						    					"tpl"=>$this->info["reserva"]["tpl"],
						    					"desde"=>$this->info["reserva"]["desde"],
						    					"hasta"=>$this->info["reserva"]["hasta"],
						    					"habitacion"=>$habitacion,
						    					"pk"=>$pk,
						    					"es_global"=>$this->info["ver"],
						    					"mira"=>$this->info["mira"],
						   						"valor_sgl"=>$sgl,
						   						"valor_dbl"=>$dbl,
						   						"valor_tpl"=>$tpl,
						   						"nombre_hotel"=>$nombre_hotel,
						   						"hotel"=>$hotel,
						   						"hab_normal"=>$hab_normal,
						   						"nombre_hab"=>$nombre_hab,
						   						//"id_agrupar"=>$pk.$habitacion
						    				   );

						    	if($this->info["ver"] == 0)
						    		$datos["id_agrupar"] = $pk.$habitacion;
						    	else
						    		$datos["id_agrupar"] = $pk.$hab_normal;


						    	array_push($this->info["hotdet"], $datos);

					    	$traer->MoveNext();

				   		}

				    }


			//}



			/*$hasta_comparar =  date("Y-m-d", strtotime($this->informacion["hasta"] ."- 1 days"));
		
			for($i=$this->informacion["desde"];$i<=$hasta_comparar;$i = date("Y-m-d", strtotime($i ."+ 1 days"))){
				$fechas_reserva[] = $i;
			}*/

			$this->info["hotdet_por_hotel"] = array();
           
            foreach($this->info["hotdet"] as $info=>$d){

                $primero = strtoupper($this->info["hotdet"][$info]["id_agrupar"]);

                if (!isset($this->info["hotdet_por_hotel"][$primero])) 
                    $this->info["hotdet_por_hotel"][$primero] = array();
                
                $this->info["hotdet_por_hotel"][$primero][$info] = $d;

            }


            $this->info["matriz"] = array();



        	$this->info["confirmacion_instantanea"] = array();
			$this->info["on_request"] = array();


            foreach ($this->info["hotdet_por_hotel"] as $key => $datos) {
            	$cont = 0;
            	$hotdet_minimo = array();

            	$valor_sgl = "";
            	$nuevo_valor = "";
            	foreach ($datos as $x => $row){
            		$hotdet_minimo[]  = $row["valor_sgl"]."-".$x;
				}

				
				$minimo = min($hotdet_minimo);
				$separar = explode("-", $minimo);
				$id_tarifa_mas_barata = $separar[1];

				foreach ($datos as $id_arreglo => $value){

					if($id_tarifa_mas_barata == $id_arreglo){

	            		$pk = $value["pk"];
						$hotel = $value["hotel"];
						$habitacion_normal = $value["hab_normal"];
						$habitacion_global = $value["habitacion"];
						$desde = date("Y-m-d",strtotime($value["desde"]));
						$hasta = date("Y-m-d",strtotime($value["hasta"]));
						$hotdet = $value["hotdet"];
						$sgl = $value["sgl"];
						$twn = $value["twn"];
						$dbl = $value["dbl"];
						$tpl = $value["tpl"];
						$nombre_hotel = $value["nombre_hotel"];
						$nombre_hab = $value["nombre_hab"];

						$this->info["valor_sgl"] = $value["valor_sgl"];
						$this->info["valor_dbl"] = $value["valor_dbl"];
						$this->info["valor_tpl"] = $value["valor_tpl"];

						
						if($value["ver"] == 0 and $value["mira"] == 1)
							$this->ver_global($pk,$hotel,$habitacion_global,$desde,$hasta,$hotdet,$sgl,$twn,$dbl,$tpl,$nombre_hotel,$nombre_hab);
						else
							$this->ver_normal($hotel,$desde,$hasta,$hotdet,$sgl,$twn,$dbl,$tpl,$nombre_hotel,$nombre_hab);
						

		           }
				
				}					

            }


            /*echo "CONFIRMACION INSTANTANEA<br><br>";

			echo "<pre>";
				print_r($this->info["confirmacion_instantanea"]);
			echo "</pre>";

			echo "<br><br>ON REQUEST<br><br>";

			echo "<pre>";
				print_r($this->info["on_request"]);
			echo "</pre>";*/




            

            /*

			              -----------------  versión trae todos los hotdet (no el más barato) ------------------------------

            die();

			$this->info["confirmacion_instantanea"] = array();
			$this->info["on_request"] = array();

			foreach ($this->info["hotdet"] as $key => $value) {


				$pk = $value["pk"];
				$hotel = $value["hotel"];
				$habitacion_normal = $value["hab_normal"];
				$habitacion_global = $value["habitacion"];
				$fecha = date("Y-m-d",strtotime($value["desde"]));
				$hotdet = $value["hotdet"];
				$sgl = $value["sgl"];
				$twn = $value["twn"];
				$dbl = $value["dbl"];
				$tpl = $value["tpl"];
				$nombre_hotel = $value["nombre_hotel"];
				$nombre_hab = $value["nombre_hab"];

				$this->info["valor_sgl"] = $value["valor_sgl"];
				$this->info["valor_dbl"] = $value["valor_dbl"];
				$this->info["valor_tpl"] = $value["valor_tpl"];

				
				if($value["ver"] == 0 and $value["mira"] == 1)
                    $this->ver_global($pk,$hotel,$habitacion_global,$fecha,$hotdet,$sgl,$twn,$dbl,$tpl,$nombre_hotel,$nombre_hab);
                /*elseif($value["ver"] == 0 and $value["mira"] == 0){
                	//echo "Hotel con dispo normal y global<br>";
                	$existe_dispo_normal = $this->revisar_normal($fecha,$hotdet,$cd_hab1,$cd_hab2,$cd_hab3,$cd_hab4);	

                	if(!$existe_dispo_normal){
                		$this->ver_global($id_pk,$hotel,$id_cot,$id_tipohabitacion,$fecha,$hotdet,$cd_hab1,$cd_hab2,$cd_hab3,$cd_hab4);
                	}

                }
			}		


			echo "CONFIRMACION INSTANTANEA<br><br>";

			echo "<pre>";
				print_r($this->info["confirmacion_instantanea"]);
			echo "</pre>";

			echo "<br><br>ON REQUEST<br><br>";

			echo "<pre>";
				print_r($this->info["on_request"]);
			echo "</pre>";*/


			//$this->datos["confirmacion_instantanea"]  = $this->info["confirmacion_instantanea"];
			//$this->datos["on_request"]  = $this->info["on_request"];


			$disponibilidad = array(
									"confirmacion_instantanea"=>$this->info["confirmacion_instantanea"],
						   			"on_request"=>$this->info["on_request"]
								);

			return $disponibilidad;

		}



		protected function buscar_mkup($id_operador,$hotel){


			switch ($this->info["cliente"]) {

				/**
					caso cocha ya que la tabla no existe en otro cliente
				*/
				case "cocha":
					$consulta = "
							SELECT 
							  * 
							FROM
							  ".$this->info["bd"].".mark_hot mh
							WHERE mh.id_operador = ".$id_operador." 
							  AND mh.id_hotel = ".$hotel."
							  AND mh.estado = 0
							  ORDER BY id_mark DESC LIMIT 1
							";	

					$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

					if($traer->RecordCount() == 0){
						
						$consulta = "
										select markup_emisivo from ".$this->info["bd"].".hotel where id_hotel = $id_operador and id_tipousuario = 3
									";	
			
						$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);
						$this->info["markup"] = $traer->Fields("markup_emisivo");
					
					}else{
						$markup_hotel = $traer->Fields("markup");
						$this->info["markup"] = $markup_hotel;
					}


				break;


				case "veci":

				        $this->info["markup"] = 0;
						
				        // id empresa como sesión (del usuario) en este caso y a modo de ejemplo dejaré la misma del operador VECI
				        $session = 3030;

				        $veci = 3030;
				        $telefonica = 3006;


				        $campo = "";

				        if($session == $veci) $campo = "hot_markup";
				        elseif($session == $telefonica) $campo = "hot_markup_telefonica";
				        else $campo = "hot_markup_empresa";

						$consulta = "
										select $campo as markup from ".$this->info["bd"].".hotel where id_hotel = $id_operador and id_tipousuario = 3
									";	
			         
						$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

						if($traer->RecordCount() > 0)
							$this->info["markup"] = $traer->Fields("markup");


				break;

				case "turavion":
				case "travelclub":

						if($this->info["area"] == 1)
							$this->info["markup"] = 1; // el mark-up se podría aplicar para receptivo solo bastaría quitar el IIIIIFFFFF
						else{
                         
                        $this->info["markup"] = 1;

                        $consulta = "select markup_emisivo from ".$this->info["bd"].".hotel where id_tipousuario=3 and codigo_cliente = '".$id_operador."' LIMIT 1";
					    $traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

						    if($traer->RecordCount() > 0)
						    	$this->info["markup"] = $traer->fields("markup_emisivo");
						    else{

									$consulta = " select * from ".$this->info["bd"].".mark_hot mh
														where mh.id_operador = ".$id_operador."  
														and mh.id_hotel = ".$hotel."
														and mh.estado = 0
														order by id_mark desc limit 1
													";
									$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

									if($traer->RecordCount() > 0)
										$this->info["markup"] = $traer->Fields("markup");
									else{

										$consulta = "select hd_markup from ".$this->info["bd"].".hotdet where id_hotdet = ".$this->info["id_hotdet_markup"];
										$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

										if($traer->RecordCount() > 0){

											if($traer->Fields("hd_markup") == 0 or $traer->Fields("hd_markup") == 1){

												$consulta = "SELECT cont_markuphtl FROM ".$this->info["bd"].".cont WHERE id_cont = 2 " ;
												$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);
												$this->info["markup"] = $traer->Fields('cont_markuphtl');

											}else 
												$this->info["markup"] = $traer->Fields('hd_markup');	 	

										}
									}
								}

						}	

				break;


				// para los clientes que ocupan mark up dinámico en la plataforma se hará el calculo directo desde markup (el valor final)

				case "carlson":
				case "mundotour":

					$es_imperativo = false;
					$markup_operador = 1;
					$markup_default = 1;

					$sgl = 0;
					$twn = 0;
					$dbl = 0;
					$tpl = 0;

					$desde = date("Y-m-d",strtotime($this->info["desde_markup"]));
					$hasta_comparar =  date("Y-m-d", strtotime($this->info["hasta_markup"] ."- 1 days"));


					// mk imperativo y saber el continente
					$consulta = "SELECT 
								  id_continente,
								  mark_ope_imp,
								  mark_operador
								FROM
								  ".$this->info["bd"].".hotel ho
								WHERE id_hotel = $id_operador";

					$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

					if($traer->RecordCount() > 0){


						if($traer->Fields("mark_ope_imp") == 0)
							$es_imperativo = true;
						else
							$markup_operador = $traer->Fields("mark_operador");



						// si el mark up del operador es 1 preguntamos por el del continente

						if($markup_operador == 1){
							
							
							$consulta = "
											SELECT o.cont_markuphtl
											FROM ".$this->info["bd"].".hotel h 
											JOIN cont o ON h.id_continente = o.id_cont 
											WHERE h.id_hotel = $id_operador 
											AND cont_estado = 0 
											AND o.area_cont = ".$this->info["area"]." 
										";

							$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

							if($traer->RecordCount() > 0)
								$markup_default = $traer->Fields("cont_markuphtl");

							// si no hay mark up continente preguntamos por el del área

							if($markup_default == 1){

								$consulta = "SELECT area_markup FROM ".$this->info["bd"].".area where id_area = ".$this->info["area"];
								$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);
								
								if($traer->RecordCount() > 0)
									$markup_default = $resmkarea->Fields('area_markup');

							}	
						}

					}


					// si no es imperativo pasamos por jerarquía

					$revisar_markup_especifico = true;



					if(!$es_imperativo){

						for($i=$desde;$i<=$hasta_comparar;$i = date("Y-m-d", strtotime($i ."+ 1 days"))){ // recorre por noches para calcular el markup por día

							$consulta = "
									 SELECT hbmk_markup_sgl, hbmk_markup_dbt, hbmk_markup_dbm, hbmk_markup_tpl 
									 FROM ".$this->info["bd"].".mk_ope_hotdet 
									 WHERE hbmk_estado = 0 
									 AND id_hotdet = ".$this->info["id_hotdet_markup"]." 
									 AND id_hot = ".$hotel." 
									 AND id_hotel = ".$id_operador." 
									 AND id_mkgrupo IS NULL 
									 AND '".$i."' 
									BETWEEN hbmk_fecdesde AND hbmk_fechasta";
						
							$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);


							if($traer->RecordCount() > 0){

								//$this->info['markup_sgl'] = $traer->Fields('hbmk_markup_sgl');
								//$this->info['markup_dbt'] = $traer->Fields('hbmk_markup_dbt');
								//$this->info['markup_dbm'] = $traer->Fields('hbmk_markup_dbm');
								//$this->info['markup_tpl'] = $traer->Fields('hbmk_markup_tpl');

								$sgl += ($this->info["valor_sgl"]/$traer->Fields('hbmk_markup_sgl'));
								$twn += ($this->info["valor_twn"]*2/$traer->Fields('hbmk_markup_dbt'));
								$dbl += ($this->info["valor_dbl"]*2/$traer->Fields('hbmk_markup_dbm'));
								$tpl += ($this->info["valor_tpl"]*3/$traer->Fields('hbmk_markup_tpl'));


								$revisar_markup_especifico = false;

							}
			
						}


						if($markup_operador!=1 and $revisar_markup_especifico){
							//$this->info['markup_sgl'] = $markup_operador;
							//$this->info['markup_twn'] = $markup_operador;
							//$this->info['markup_dbl'] = $markup_operador;
							//$this->info['markup_tpl'] = $markup_operador;

							$sgl = ($this->info["valor_sgl"]/$markup_operador);
							$twn = ($this->info["valor_twn"]*2/$markup_operador);
							$dbl = ($this->info["valor_dbl"]*2/$markup_operador);
							$tpl = ($this->info["valor_tpl"]*3/$markup_operador);




						//si no pasa trata de buscar en los eslabones que siguen
						}else{
							//si el markup general de tarifa no es 1, setea el arreglo de markups y flaguea en falso para que deje de buscar.
							if($revisar_markup_especifico and $disp->Fields('hd_markup')!=1){
								//$this->info['markup_sgl'] = $disp->Fields('hd_markup');
								//$this->info['markup_twn'] = $disp->Fields('hd_markup');
								//$this->info['markup_dbl'] = $disp->Fields('hd_markup');
								//$this->info['markup_tpl'] = $disp->Fields('hd_markup');
								
								$sgl = ($this->info["valor_sgl"]/$disp->Fields('hd_markup'));
								$twn = ($this->info["valor_twn"]*2/$disp->Fields('hd_markup'));
								$dbl = ($this->info["valor_dbl"]*2/$disp->Fields('hd_markup'));
								$tpl = ($this->info["valor_tpl"]*3/$disp->Fields('hd_markup'));


								$revisar_markup = false;
							}
							//si el markup general de hotel no es 1, setea el arreglo de markups y flaguea en falso para que deje de buscar.
							if($revisar_markup_especifico){
								
									$consulta = "SELECT markdin_valor FROM ".$this->info["bd"].".markdin_gral WHERE id_area = ".$this->info["area"]." AND id_hotel = $hotel AND markdin_estado = 0";
									$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);
									
									if($traer->RecordCount()>0){
										//$this->info['markup_sgl'] = $traer->Fields('markdin_valor');
										//$this->info['markup_twn'] = $traer->Fields('markdin_valor');
										//$this->info['markup_dbl'] = $traer->Fields('markdin_valor');
										//$this->info['markup_tpl'] = $traer->Fields('markdin_valor');

										$sgl = ($this->info["valor_sgl"]/$disp->Fields('markdin_valor'));
										$twn = ($this->info["valor_twn"]*2/$disp->Fields('markdin_valor'));
										$dbl = ($this->info["valor_dbl"]*2/$disp->Fields('markdin_valor'));
										$tpl = ($this->info["valor_tpl"]*3/$disp->Fields('markdin_valor'));

									}
								
							}
							//Si no encontró nada, setea el arreglo con el markup por default
							if($revisar_markup_especifico){
								//$this->info['markup_sgl'] = $markup_default;
								//$this->info['markup_twn'] = $markup_default;
								//$this->info['markup_dbl'] = $markup_default;
								//$this->info['markup_tpl'] = $markup_default;

								$sgl = ($this->info["valor_sgl"]/$markup_default);
								$twn = ($this->info["valor_twn"]*2/$markup_default);
								$dbl = ($this->info["valor_dbl"]*2/$markup_default);
								$tpl = ($this->info["valor_tpl"]*3/$markup_default);
							}
						}

					}else{
					
						//$this->info['markup_sgl'] = $markup_operador;
						//$this->info['markup_twn'] = $markup_operador;
						//$this->info['markup_dbl'] = $markup_operador;
						//$this->info['markup_tpl'] = $markup_operador;

						$sgl = ($this->info["valor_sgl"]/$markup_operador);
						$twn = ($this->info["valor_twn"]*2/$markup_operador);
						$dbl = ($this->info["valor_dbl"]*2/$markup_operador);
						$tpl = ($this->info["valor_tpl"]*3/$markup_operador);


					}


					$this->info["valor_sgl"] = $sgl;
					$this->info["valor_dbl"] = $dbl;
					$this->info["valor_tpl"] = $tpl;		


				break;


			}


			

		}


		/**
			SOLO EN OTSI
		**/
		protected function comision($com_tipo,$id_grupo){   

				$consulta = "SELECT com_comag FROM comision WHERE com_tipo = '$com_tipo' and id_grupo = $id_grupo";
				//echo $consulta;
				$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);
				if($traer ->Fields('com_comag')==''){
					$comision = 0;
				}else{
					$comision = $traer ->Fields('com_comag');
				}
			
			return $comision;
		}


		protected function ver_global($id_pk,$hotel,$habitacion,$desde,$hasta,$hotdet,$hab1,$hab2,$hab3,$hab4,$nombre_hotel,$nombre_hab){

				$hasta_comparar =  date("Y-m-d", strtotime($hasta ."- 1 days"));
				$cant_hab = 0;
				$id_stock_global = "";	
				for($fecha=$desde;$fecha<=$hasta_comparar;$fecha = date("Y-m-d", strtotime($fecha ."+ 1 days"))){
					$cant_hab+=1;

					$id_stock_global_return = $this->traer_id_stock($id_pk,$fecha,$habitacion);

					if(!empty($id_stock_global_return))
			    		$id_stock_global .= $id_stock_global_return.",";
			    }

			    $id_stock_global = substr($id_stock_global, 0, -1);


					$this->info["total_normal_sgl"] = 0;
					$this->info["total_normal_twn"] = 0;
					$this->info["total_normal_dbl"] = 0;
					$this->info["total_normal_tpl"] = 0;

					if($id_stock_global == ""){

							$datos = array(
				    						"desde"=>$desde,
				    						"hasta"=>$hasta,
				    						"hotdet"=>$hotdet,
				    						"hotel"=>$hotel,
				    						"nombre_hotel"=>$nombre_hotel,
				    						"nombre_hab"=>$nombre_hab,
				    						"valor_sgl"=>$this->info["valor_sgl"]*$cant_hab,
				    						"valor_dbl"=>$this->info["valor_dbl"]*$cant_hab,
				    						"valor_tpl"=>$this->info["valor_tpl"]*$cant_hab
				    					  );

				    		array_push($this->info["on_request"], $datos);


					}else{
				
				    	$existe_global = $this->revisar_dispo_global($hab1,$hab2,$hab3,$hab4,$id_stock_global,$desde,$hasta,$hotdet,$habitacion,$id_pk);

				    	if($existe_global){

				    		$datos = array(
				    						"desde"=>$desde,
				    						"hasta"=>$hasta,
				    						"hotdet"=>$hotdet,
				    						"hotel"=>$hotel,
				    						"nombre_hotel"=>$nombre_hotel,
				    						"nombre_hab"=>$nombre_hab,
				    						"valor_sgl"=>$this->info["valor_sgl"]*$cant_hab,
				    						"valor_dbl"=>$this->info["valor_dbl"]*$cant_hab,
				    						"valor_tpl"=>$this->info["valor_tpl"]*$cant_hab
				    					  );

				    		array_push($this->info["confirmacion_instantanea"], $datos);

				    	}else{


				    		$datos = array(
				    						"desde"=>$desde,
				    						"hasta"=>$hasta,
				    						"hotel"=>$hotel,
				    						"nombre_hotel"=>$nombre_hotel,
				    						"nombre_hab"=>$nombre_hab,
				    						"valor_sgl"=>$this->info["valor_sgl"]*$cant_hab,
				    						"valor_dbl"=>$this->info["valor_dbl"]*$cant_hab,
				    						"valor_tpl"=>$this->info["valor_tpl"]*$cant_hab
				    					  );

				    		array_push($this->info["on_request"], $datos);

				    	}

				    }


    	}



    	protected function ver_normal($hotel,$desde,$hasta,$hotdet,$hab1,$hab2,$hab3,$hab4,$nombre_hotel,$nombre_hab){

				$hasta_comparar =  date("Y-m-d", strtotime($hasta ."- 1 days"));
				$cant_hab = 0;
				for($fecha=$desde;$fecha<=$hasta_comparar;$fecha = date("Y-m-d", strtotime($fecha ."+ 1 days"))){
					$cant_hab+=1;
			    }

				
			    	$existe_normal = $this->revisar_normal($desde,$hasta,$hotdet,$hab1,$hab2,$hab3,$hab4);

			    	if($existe_normal){

			    		$datos = array(
			    						"desde"=>$desde,
			    						"hasta"=>$hasta,
			    						"hotdet"=>$hotdet,
			    						"hotel"=>$hotel,
			    						"nombre_hotel"=>$nombre_hotel,
			    						"nombre_hab"=>$nombre_hab,
			    						"valor_sgl"=>$this->info["valor_sgl"]*$cant_hab,
			    						"valor_dbl"=>$this->info["valor_dbl"]*$cant_hab,
			    						"valor_tpl"=>$this->info["valor_tpl"]*$cant_hab
			    					  );

			    		array_push($this->info["confirmacion_instantanea"], $datos);

			    	}else{


			    		$datos = array(
			    						"desde"=>$desde,
			    						"hasta"=>$hasta,
			    						"hotel"=>$hotel,
			    						"nombre_hotel"=>$nombre_hotel,
			    						"nombre_hab"=>$nombre_hab,
			    						"valor_sgl"=>$this->info["valor_sgl"]*$cant_hab,
			    						"valor_dbl"=>$this->info["valor_dbl"]*$cant_hab,
			    						"valor_tpl"=>$this->info["valor_tpl"]*$cant_hab
			    					  );

			    		array_push($this->info["on_request"], $datos);

			    	}

    	}




		 protected function revisar_dispo_global($cd_hab1,$cd_hab2,$cd_hab3,$cd_hab4,$id_stock_global,$desde,$hasta,$hotdet,$habitacion,$pk){

		        $modo = $this->buscar_modo($pk);

		        $stock_global_sgl = $this->stock_global(1,$desde,$hasta,$id_stock_global);
		        $stock_global_twn = $this->stock_global(2,$desde,$hasta,$id_stock_global);
		        $stock_global_dbl = $this->stock_global(3,$desde,$hasta,$id_stock_global);
		        $stock_global_tpl = $this->stock_global(4,$desde,$hasta,$id_stock_global);


		        $hotocu_global_sgl = $this->hotocu_global(1,$desde,$hasta,$id_stock_global,$habitacion);
		        $hotocu_global_twn = $this->hotocu_global(2,$desde,$hasta,$id_stock_global,$habitacion);
		        $hotocu_global_dbl = $this->hotocu_global(3,$desde,$hasta,$id_stock_global,$habitacion);
		        $hotocu_global_tpl = $this->hotocu_global(4,$desde,$hasta,$id_stock_global,$habitacion);


		        if($modo == 0){ 


		                $dispo_global_sgl = $stock_global_sgl - $hotocu_global_sgl;
		                $dispo_global_twn = $stock_global_twn - $hotocu_global_twn;
		                $dispo_global_dbl = $stock_global_dbl - $hotocu_global_dbl;
		                $dispo_global_tpl = $stock_global_tpl - $hotocu_global_tpl;


		                if($dispo_global_sgl<0) $dispo_global_sgl = 0;
						if($dispo_global_twn<0) $dispo_global_twn = 0;
						if($dispo_global_dbl<0) $dispo_global_dbl = 0;
						if($dispo_global_tpl<0) $dispo_global_tpl = 0;
		               

		                $disp_n_g_sgl = $this->info["total_normal_sgl"] + $dispo_global_sgl;
						$disp_n_g_twn = $this->info["total_normal_twn"] + $dispo_global_twn;
						$disp_n_g_dbl = $this->info["total_normal_dbl"] + $dispo_global_dbl;
						$disp_n_g_tpl = $this->info["total_normal_tpl"] + $dispo_global_tpl;

		                //echo $stock_global_twn."<=>".$hotocu_global_twn."<=>".$dispo_global_twn.">===<br>";

		                /*echo "GLOBAL SGL=>$cd_hab1 ::".$this->datos["total_normal_sgl"]."<=>".$stock_global_sgl."=>".$hotocu_global_sgl."<br>";
		                echo "GLOBAL TWN=>$cd_hab2 ::".$this->datos["total_normal_twn"]."<=>".$stock_global_twn."=>".$hotocu_global_twn."<br>";
		                echo "GLOBAL DBL=>$cd_hab3 ::".$this->datos["total_normal_dbl"]."<=>".$stock_global_dbl."=>".$hotocu_global_dbl."<br>";
		                echo "GLOBAL TPL=>$cd_hab4 ::".$this->datos["total_normal_tpl"]."<=>".$stock_global_tpl."=>".$hotocu_global_tpl."<br>";

		                if($disp_n_g_sgl>=$cd_hab1)
		                	echo "SI SGL <br>";

		                if($disp_n_g_twn>=$cd_hab2)
		                	echo "SI TWN <br>";

		                if($disp_n_g_dbl>=$cd_hab3)
		                	echo "SI DBL <br>";

		                if($disp_n_g_tpl>=$cd_hab4)
		                	echo "SI TPL <br>";*/



		                $this->info["total_global_sgl"] = $dispo_global_sgl;
		                $this->info["total_global_twn"] = $dispo_global_twn;
		                $this->info["total_global_dbl"] = $dispo_global_dbl;
		                $this->info["total_global_tpl"] = $dispo_global_tpl;



		                if($disp_n_g_sgl>=$cd_hab1 && $disp_n_g_twn>=$cd_hab2 && $disp_n_g_dbl>=$cd_hab3 && $disp_n_g_tpl>=$cd_hab4)
		                    $respuesta = true;
		                else
		                    $respuesta = false;

		        }else{

		            $dispo_normal_sgl = $this->info["total_normal_sgl"];

		            if($dispo_normal_sgl<0)
		                $dispo_normal_sgl = 0;

		            $dispo_global_sgl = $stock_global_sgl*1 - $hotocu_global_sgl*1;


		            if($dispo_global_sgl<0)
		                $dispo_global_sgl = 0;


		            $disp_n_g_sgl = $dispo_normal_sgl + $dispo_global_sgl;

		            $total_pedido_hab = $cd_hab1 + $cd_hab2 + $cd_hab3 + $cd_hab4;



		            if($disp_n_g_sgl>=$total_pedido_hab)
		                $respuesta = true;
		            else
		                $respuesta = false;
		         
		        }


		        return $respuesta;
		    }




	    protected function revisar_normal($desde,$hasta,$hotdet,$cd_hab1,$cd_hab2,$cd_hab3,$cd_hab4){

			$stock_sgl = $this->stock_normal(1,$desde,$hasta,$hotdet);
	        $stock_twn = $this->stock_normal(2,$desde,$hasta,$hotdet);
	        $stock_dbl = $this->stock_normal(3,$desde,$hasta,$hotdet);
	        $stock_tpl = $this->stock_normal(4,$desde,$hasta,$hotdet);


	        $hotocu_sgl = $this->hotocu_normal(1,$hotdet,$desde,$hasta);
	        $hotocu_twn = $this->hotocu_normal(2,$hotdet,$desde,$hasta);
	        $hotocu_dbl = $this->hotocu_normal(3,$hotdet,$desde,$hasta);
	        $hotocu_tpl = $this->hotocu_normal(4,$hotdet,$desde,$hasta);

	    

		        $dispo_normal_sgl = ($stock_sgl*1 - $hotocu_sgl*1);
		        $dispo_normal_twn = ($stock_twn*1 - $hotocu_twn*1);
		        $dispo_normal_dbl = ($stock_dbl*1 - $hotocu_dbl*1);
		        $dispo_normal_tpl = ($stock_tpl*1 - $hotocu_tpl*1);

		        //echo "NORMAL DISPO SGL => ".$dispo_normal_sgl."<=>".$cd_hab1."<=>".$stock_sgl."<=>".$hotocu_sgl."<br>";
		        //echo "NORMAL DISPO TWN => ".$dispo_normal_twn."<=>".$cd_hab2."<=>".$stock_twn."<=>".$hotocu_twn."<br>";
		        //echo "NORMAL DISPO DBL => ".$dispo_normal_dbl."<=>".$cd_hab3."<=>".$stock_dbl."<=>".$hotocu_dbl."<br>";
		        //echo "NORMAL DISPO TPL => ".$dispo_normal_tpl."<=>".$cd_hab4."<=>".$stock_tpl."<=>".$hotocu_tpl."<br>";



	        if($dispo_normal_sgl<0) $dispo_normal_sgl = 0;
			if($dispo_normal_twn<0) $dispo_normal_twn = 0;
			if($dispo_normal_dbl<0) $dispo_normal_dbl = 0;
			if($dispo_normal_tpl<0) $dispo_normal_tpl = 0;


	        $this->info["total_normal_sgl"] = $dispo_normal_sgl;
	        $this->info["total_normal_twn"] = $dispo_normal_twn;
	        $this->info["total_normal_dbl"] = $dispo_normal_dbl;
	        $this->info["total_normal_tpl"] = $dispo_normal_tpl;

	        $this->info["stock_normal_sgl"] = $stock_sgl;
	        $this->info["stock_normal_twn"] = $stock_twn;
	        $this->info["stock_normal_dbl"] = $stock_dbl;
	        $this->info["stock_normal_tpl"] = $stock_tpl;



	         //echo "DISPO NORMAL : ====>".$dispo_normal_sgl."<=>".$dispo_normal_twn."<=>".$dispo_normal_dbl."<=>".$dispo_normal_tpl."<br>";

	         if($dispo_normal_sgl>=$cd_hab1 && $dispo_normal_twn>=$cd_hab2 && $dispo_normal_dbl>=$cd_hab3 && $dispo_normal_tpl>=$cd_hab4)
	       	 	$normal = true;
	       	 else
	            $normal = false;


	        return $normal;
	    }


		protected function buscar_modo($pk){

	        $consulta = "select id_cadena from hoteles.hotelesmerge where id_pk = $pk ";
	        $traer = $this->sql_con->SelectLimit($consulta);

	        $cadena  = $traer->Fields("id_cadena");

	        $con = "select modo from hoteles.cadena where id_cadena = $cadena ";
	        $ver = $this->sql_con->SelectLimit($con);

	        return $ver->Fields("modo");

	    }



		 protected function stock_normal($tipo,$fecha,$hotdet){

    	$consulta = "";

        switch ($tipo) {

                case 1:
                  
                   $consulta = "select sum(sc_hab1) as valor from distantis.stock where sc_fecha between '$desde' and '$hasta' and id_hotdet = $hotdet  and sc_estado = 0";

                break;

                case 2:
                  
                   $consulta = "select sum(sc_hab2) as valor from distantis.stock where sc_fecha between '$desde' and '$hasta' and id_hotdet = $hotdet  and sc_estado = 0";

                break;

                case 3:
                  
                   $consulta = "select sum(sc_hab3) as valor from distantis.stock where sc_fecha between '$desde' and '$hasta' and id_hotdet = $hotdet  and sc_estado = 0";

                break;

                case 4:
                  
                   $consulta = "select sum(sc_hab4) as valor from distantis.stock where sc_fecha between '$desde' and '$hasta' and id_hotdet = $hotdet  and sc_estado = 0";

                break;
            
            
        }

       $traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

       $valor = $traer->Fields("valor");

        if($valor < 0)
            $valor = 0;

        return $valor;

    }


    protected function hotocu_normal($tipo,$hotdet,$fecha){

    	$consulta = "";

        switch ($tipo) {

                case 1:
                  
                   $consulta = "select sum(hc_hab1) as valor from hotocu where hc_fecha between '$desde' and '$hasta' and id_hotdet = $hotdet and hc_estado = 0";

                break;

                case 2:
                  
                   $consulta = "select sum(hc_hab2) as valor from hotocu where hc_fecha between '$desde' and '$hasta' and id_hotdet = $hotdet and hc_estado = 0";

                break;

                case 3:
                  
                   $consulta = "select sum(hc_hab3) as valor from hotocu where hc_fecha between '$desde' and '$hasta' and id_hotdet = $hotdet and hc_estado = 0";

                break;

                case 4:
                  
                   $consulta = "select sum(hc_hab4) as valor from hotocu where hc_fecha between '$desde' and '$hasta' and id_hotdet = $hotdet and hc_estado = 0";

                break;
            
            
        }

        $traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

        $valor = $traer->Fields("valor");

        if($valor < 0)
            $valor = 0;


        return $valor;

    }


    protected function stock_global($tipo,$desde,$hasta,$id_stock_global){

    	$consulta = "";

        switch ($tipo) {

                case 1:
                  
                   $consulta = "select sum(sc_hab1) as valor from hoteles.stock_global where sc_fecha between '$desde' and '$hasta' and id_stock_global in ($id_stock_global) and  sc_estado= 0";

                break;

                case 2:
                  
                   $consulta = "select sum(sc_hab2) as valor from hoteles.stock_global where sc_fecha between '$desde' and '$hasta' and id_stock_global in ($id_stock_global) and  sc_estado= 0";

                break;

                case 3:
                  
                   $consulta = "select sum(sc_hab3) as valor from hoteles.stock_global where sc_fecha between '$desde' and '$hasta' and id_stock_global in ($id_stock_global) and  sc_estado= 0";

                break;

                case 4:
                  
                   $consulta = "select sum(sc_hab4) as valor from hoteles.stock_global where sc_fecha between '$desde' and '$hasta' and id_stock_global in ($id_stock_global) and  sc_estado= 0";

                break;
            
            
        }


        $traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);


        return $traer->Fields("valor");

    }


    protected function hotocu_global($tipo,$desde,$hasta,$id_stock_global,$habitacion){

    	$consulta = "";

        switch ($tipo) {

                case 1:
                  
                   $consulta = "select sum(hc_hab1) as valor from hoteles.hotocu where hc_fecha between '$desde' and '$hasta' and id_stock_global in ($id_stock_global) and hc_estado = 0";

                break;

                case 2:
                  
                   $consulta = "select sum(hc_hab2) as valor from hoteles.hotocu where hc_fecha between '$desde' and '$hasta' and id_stock_global in ($id_stock_global) and hc_estado = 0";

                break;

                case 3:
                  
                   $consulta = "select sum(hc_hab3) as valor from hoteles.hotocu where hc_fecha between '$desde' and '$hasta' and id_stock_global in ($id_stock_global) and hc_estado = 0";

                break;

                case 4:
                  
                   $consulta = "select sum(hc_hab4) as valor from hoteles.hotocu where hc_fecha between '$desde' and '$hasta' and id_stock_global in ($id_stock_global) and hc_estado = 0";

                break;
            
            
        }


        $traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

        $valor = $traer->Fields("valor");


        if($valor < 0)
            $valor = 0;


        return $valor;

    }


    protected function traer_id_stock($pk,$fecha,$habitacion){

        $consulta = "select id_stock_global from hoteles.stock_global where id_pk = $pk and sc_fecha = '$fecha' and id_tipohab = $habitacion";
        $traer = $this->sql_con->SelectLimit($consulta);

        return $traer->Fields("id_stock_global");

    }


    protected function es_global($pk){

    	$this->info["mira"] = 100;
        $this->info["ver"] = 100;

    	if($pk != ""){

	        $consulta = "select ver,mira from hoteles.hotelesmerge where id_pk = $pk ";
	        $traer = $this->sql_con->SelectLimit($consulta);

	        $this->info["mira"] = $traer->Fields("mira");
	        $this->info["ver"] = $traer->Fields("ver");

	    }

    }



    protected function traer_pk($hotel){

        $consulta = "select * from hoteles.hotelesmerge where id_hotel_".$this->info["cliente"]." = $hotel";
        $traer = $this->sql_con->SelectLimit($consulta);

        return $traer->Fields("id_pk");

    }

     protected function buscar_bd(){

        $consulta = "select bd from hoteles.clientes where nombre ='".$this->info["cliente"]."'  ";
        $traer = $this->sql_con->SelectLimit($consulta);

        $this->info["bd"] = $traer->Fields("bd");

    }

    protected function errores($linea){
        
        die($_SERVER['REQUEST_URI']." - ".$linea." : ".$this->sql_con->ErrorMsg());
        
    }



	function __destruct(){
		$this->sql_con->Close();
		//echo json_encode($this->datos);
	}
	


	}



 ?>