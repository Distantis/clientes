<?php

$cliente = new Cliente();

class Cliente{
	protected $sql_con,$matriz;
	protected $datos = array();
	protected $info = array();
	protected $session = array();

	public function __construct(){
		
		error_reporting(0);
		session_start();
		require_once('/var/www/h2o/Connections/db1.php');
		require_once('/var/www/clientes/production/sitio/Matriz.php');
		$this->conectar($db1);
		$this->obtener_info();
	}

	protected function conectar($db1){
		$this->sql_con = $db1;
	}

	protected function obtener_info(){

		extract($_POST);

		foreach ($_SESSION as $key => $value) {
			$this->session["".$key.""] = $value;
		}

		foreach ($_POST as $key => $value) {
			$this->info["".$key.""] = $value;
		}


		switch ($this->info["tipo"]) {
			case 1:
				$this->buscar_disponibilidad();	
			break;

			case 2:
				$this->fecha_maxima_tarifa_areas();	
			break;
		}


	}


	protected function buscar_disponibilidad(){

		$this->info["reserva"] = array();
		foreach($this->info["servicio"] as $id=>$valor){
			foreach($valor as $key=>$value){

				if($key == "desde" or $key == "hasta") $value = date("Y-m-d", strtotime($value));
				
				$this->info["reserva"][$key] = $value;
			}
		}

		$this->matriz = new Matriz($this->sql_con,$this->info["cliente"]);
		$revisar_reserva = $this->matriz->Disponibilidad($this->info["reserva"]);

		$this->datos["confirmacion_instantanea"] = $revisar_reserva["confirmacion_instantanea"];
		$this->datos["on_request"] = $revisar_reserva["on_request"];

    }



    protected function fecha_maxima_tarifa_areas(){

    	$this->datos["area"] = array();

    	switch ($this->session["cliente"]) {
    		case 'cocha':
   
    				$datos1 = array(
							  "id"=>150,
							  "nombre"=>"Vacacional"	  
    						);

    				$datos2 = array(
    							"id"=>27,
    							 "nombre"=>"Corporativa"
    						);

    				$datos3 = array(
    							"id"=>1,
    							"nombre"=>"Receptivo"
    							);

    				 array_push($this->datos["area"],$datos1);
    				 array_push($this->datos["area"],$datos2);
    				 array_push($this->datos["area"],$datos3);

    			break;

    			default:
   
    				$datos1 = array(
							  "id"=>1,
							  "nombre"=>"Receptivo"	  
    						);

    				$datos2 = array(
    							"id"=>2,
    							"nombre"=>"Emisivo/Nacional"
    							);

    				 array_push($this->datos["area"],$datos1);
    				 array_push($this->datos["area"],$datos2);

    			break;
    		
    	}


    }


	protected function errores($linea){
	  die($_SERVER['REQUEST_URI']." - ".$linea." : ".$this->sql_con->ErrorMsg());
	}


	public function __destruct(){
		$this->sql_con->close();
		echo json_encode($this->datos);
	}


}