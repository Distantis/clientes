<?php

$data = new Data();

class Data{
	protected $sql_con;
	protected $datos = array();
	protected $info = array();
	protected $session = array();

	public function __construct(){
		error_reporting(0);
		session_start();
		require_once('/var/www/h2o/Connections/db1.php');
		$this->conectar($db1);
		$this->obtener_info();
	}

	protected function conectar($db1){
		$this->sql_con = $db1;
	}

	protected function obtener_info(){

		extract($_POST);

		foreach ($_SESSION as $key => $value) {
			$this->session["".$key.""] = $value;
		}

		foreach ($_POST as $key => $value) {

			if($key == "desde" or $key == "hasta" and ($value!=""))
				$value = date("Y-m-d", strtotime($value));

			$this->info["".$key.""] = $value;
		}

		$this->buscar_bd();


		switch ($this->info["tipo"]) {
			case 1:
				$this->buscar_cotizaciones();	
			break;

			case 2:
				$this->buscar_tarifas_por_hotel();	
			break;

			case 3:
				$this->buscar_tipos_tarifa();	
			break;

			case 4:
				$this->buscar_tipohabitacion();	
			break;

			case 5:
				$this->buscar_tarifa_editar();	
			break;

			case 6:
				$this->buscar_tarifas_vencidas();	
			break;

		}
	}


	protected function buscar_tarifa_editar(){

		$consulta = "
					 select hd.*,id_tipohab from ".$this->info["bd"].".hotdet hd 
					  join ".$this->info["bd"].".tipohabitacion th 
					    on th.id_tipohabitacion = hd.id_tipohabitacion 
					 where id_hotdet = ".$this->info["tarifa"]."
					";				

	    //echo $consulta;
	    //return false;
		$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);


		$this->datos["tarifa_cliente"] = array();

		foreach($traer as $key=>$valor){
            foreach($valor as $key=>$contenido){

                if(!is_numeric($key)){

                    if($key=="hd_sgl" || $key=="hd_dbl" || $key=="hd_tpl")
                        $contenido = (int)$contenido;
                    elseif($key=="hd_fecdesde" || $key=="hd_fechasta")
                        $contenido = date('d-m-Y', strtotime($contenido));
                
                    $this->datos["tarifa_cliente"][$key] = $contenido;

                }


            }

        }		

	}


	protected function buscar_tipohabitacion(){

		$consulta = "
					 select th.id_tipohabitacion,th_nombre from hoteles.tipohabitacion th 
					   join hoteles.hotdet hd 
					    on hd.id_tipohabitacion = th.id_tipohabitacion 
					 where th_estado = 0
					 and hd.id_pk = ".$this->info["pk"]."
					 group by th.id_tipohabitacion
					";				

	    //echo $consulta;
	    //return false;
		$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);


		$this->datos["tipo_habitacion"] = array();

		while(!$traer->EOF){	

			$id_tipohabitacion = $traer->Fields("id_tipohabitacion");
			$nombre_tipohab = utf8_encode(trim($traer->Fields("th_nombre")));
			

			$datos = array(
							"nombre_tipohab"=>$nombre_tipohab,
							"id_tipohabitacion"=>$id_tipohabitacion
						);

			array_push($this->datos["tipo_habitacion"],$datos);

			$traer->MoveNext();
		}	

	}



	protected function buscar_tipos_tarifa(){

		$consulta = "
					 select * from ".$this->info["bd"].".tipotarifa tt 
					 where tt_estado = 0
					";				

	    //echo $consulta;
	    //return false;
		$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);


		$this->datos["tipo_tarifas"] = array();

		while(!$traer->EOF){	

			$id_tipotarifa = $traer->Fields("id_tipotarifa");
			$tipo_tarifa = utf8_encode(trim($traer->Fields("tt_nombre")));
			

			$datos = array(
							"tipo_tarifa"=>$tipo_tarifa,
							"id_tipotarifa"=>$id_tipotarifa,
						);

			array_push($this->datos["tipo_tarifas"],$datos);

			$traer->MoveNext();
		}	

	}

	

	protected function buscar_tarifas_por_hotel(){
		$hoy = date("Y-m-d");

		$consulta = "
					 select * from ".$this->info["bd"].".hotdet hd
					   join ".$this->info["bd"].".tipotarifa tt 
					   	on tt.id_tipotarifa = hd.id_tipotarifa
					  where hd_estado = 0 and hd_fechasta>='$hoy' 
					  and id_hotel = ".$this->info["id_hotel"]."
					";

	    //echo $consulta;
	    //return false;
		$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);


		$this->datos["tarifas"] = array();

		while(!$traer->EOF){	

			$tipo_tarifa = $traer->Fields("tt_nombre");
			$id_hotdet = $traer->Fields("id_hotdet");
			$desde = date("d-m-Y",strtotime($traer->Fields("hd_fecdesde")));
			$hasta = date("d-m-Y",strtotime($traer->Fields("hd_fechasta")));
			$sgl = round($traer->Fields("hd_sgl"),2);
			$twn = round($traer->Fields("hd_dbl")*2,2);
			$dbl = round($traer->Fields("hd_dbl")*2,2);
			$tpl = round($traer->Fields("hd_tpl")*3,2);
			$pk = $this->buscar_pk();

			$datos = array(
							"tipo_tarifa"=>$tipo_tarifa,
							"id_hotdet"=>$id_hotdet,
							"desde"=>$desde,
							"hasta"=>$hasta,
							"sgl"=>$sgl,
							"twn"=>$twn,
							"dbl"=>$dbl,
							"tpl"=>$tpl,
							"id_pk"=>$pk
						);

			array_push($this->datos["tarifas"],$datos);

			$traer->MoveNext();
		}	

	}



	protected function buscar_cotizaciones(){

		$hoy = date("Y-m-d");

		//echo "<pre>";
			//print_r($this->info);
		//echo "</pre>";

		$buscar = "";

		if($this->info["desde"] != "" and $this->info["hasta"] != "")
			$buscar .= " and cot_fecconf between '".$this->info["desde"]."' and '".$this->info["hasta"]."' ";

		if($this->info["estado"] != "")
			$buscar.=" and cot_estado = ".$this->info["estado"]." ";

		if($this->info["hotel"] != ""){
			$hotel = $this->buscar_hotel();
			$buscar.=" and cd.id_hotel = $hotel ";
		}

		if($this->info["cot"] != "")
			$buscar.=" and c.id_cot = ".$this->info["cot"]." ";
		

		if($this->info["file"] != "")
			$buscar.=" and cot_correlativo = ".$this->info["file"]." ";

		if($this->info["pax"] != ""){

			$pasajero = explode(" ",$this->info["pax"]);
			$nombre = $pasajero[0];
			$app = $pasajero[1];

			if(count($pasajero) > 1)
				$buscar.=" and (cp.cp_nombres like '%".$nombre."%' or (cp.cp_apellidos like '%".$app."%' ) ) ";
			else
				$buscar.=" and (cp.cp_nombres like '%".$nombre."%') ";

		}


		if($this->info["ciudad"] != "")
			$buscar.=" and cd.id_ciudad = ".$this->info["ciudad"]." ";



		$consulta = "select  * from ".$this->info["bd"].".cot c 
		       		join ".$this->info["bd"].".cotdes cd
		       			on cd.id_cot = c.id_cot
		       		join ".$this->info["bd"].".cotpas cp
		       			on cp.id_cot = c.id_cot
		       		join ".$this->info["bd"].".hotel ho
		       			on ho.id_hotel = cd.id_hotel
					where 

					c.id_seg in(7,13)  
					$buscar

					GROUP BY c.id_cot order by c.cot_fec DESC

					";

	    //echo $consulta;
	    //return false;
		$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);


		$this->datos["cotizaciones"] = array();

		while(!$traer->EOF){	

			$fecha_cot = date("d-m-Y",strtotime($traer->Fields("cot_fec")));
			$desde = date("d-m-Y",strtotime($traer->Fields("cd_fecdesde")));
			$hasta = date("d-m-Y",strtotime($traer->Fields("cd_fechasta")));
			$estado = $traer->Fields("cot_estado");
			$valor = $traer->Fields("cot_valor");
			$operador = $this->buscar_operador($traer->Fields("id_operador"));
			$nmr_confirmacion = $traer->Fields("cd_numreserva");
			$pax = utf8_encode(trim($traer->Fields("cp_nombres")))." ".utf8_encode(trim($traer->Fields("cp_apellidos")));
			$or = $traer->Fields("id_seg");
			$tma = $traer->Fields("cot_correlativo");
			$cot = $traer->Fields("id_cot");
			$usuario_creador = $this->buscar_usuario_creador($traer->Fields("id_usuario"));


			if($nmr_confirmacion == null) $nmr_confirmacion = "";
			
			$datos = array(
							"fecha_cot"=>$fecha_cot,
							"desde"=>$desde,
							"hasta"=>$hasta,
							"hotel"=>utf8_encode(trim($traer->Fields("hot_nombre"))),
							"estado"=>$estado,
							"valor"=>$valor,
							"operador"=>$operador,
							"nmr_confirmacion"=>$nmr_confirmacion,
							"pax"=>$pax,
							"or"=>$or,
							"tma"=>$tma,
							"id_cot"=>$cot,
							"usuario_creador"=>$usuario_creador
						);

			switch ($this->info["bd"]) {
				case 'cocha':
				    $nemo = $this->buscar_nemo($traer->Fields("nemo"));
					$datos["nemo"]=$nemo;
				break;
			}

			array_push($this->datos["cotizaciones"],$datos);

			$traer->MoveNext();
		}


	}



	protected function buscar_tarifas_vencidas(){


          $nombre = $this->session["cliente"];
          $bd = $this->info["bd"];

          $this->datos["informacion"] = array();


          if($nombre == "cocha"){

            $parte = "and hd.id_tipotarifa2 = ".$this->info["area"]." ";

            if($this->info["area"] == 1)
              $parte = "and hd.id_area = ".$this->info["area"]." ";

    

            $consulta = "
                      SELECT 
                        hm.id_pk,
                        ho.hot_nombre,
                        MAX(hd.id_hotdet) as id_hotdet,
                        MAX(hd_fechasta) AS fecha,
                        area_nombre as id_area,
                        ciu_nombre
                      FROM
                        hoteles.hotelesmerge hm
                       JOIN $bd.hotdet hd
                       ON hd.id_hotel = hm.id_hotel_$nombre 
                       join $bd.hotel ho 
                       on ho.id_hotel = hd.id_hotel
                       join $bd.area a
                       on a.id_area = hd.id_area
                       join $bd.ciudad ciu 
                       on ciu.id_ciudad = ho.id_ciudad
                      WHERE id_hotel_$nombre != 0
                      AND hd.hd_estado = 0
                      AND id_cadena != 2
                      $parte
                      and hd.id_tipotarifa not in(146,147)
                      and ho.hot_estado = 0
                      and ho.hot_activo = 0
                      GROUP BY hm.id_pk
                      ORDER BY hot_nombre
                      ";

          }elseif($nombre=="otsi"){

              $consulta = "
                       SELECT 
                          hm.id_pk,
                          ho.hot_nombre,
                          MAX(hd.id_hotdet) as id_hotdet,
                          MAX(hd_fechasta) AS fecha,
                          area_nombre as id_area,
                          ciu_nombre
                        FROM
                          hoteles.hotelesmerge hm
                         JOIN $bd.hotdet hd
                         ON hd.id_hotel = hm.id_hotel_$nombre 
                         join $bd.hotel ho 
                         on ho.id_hotel = hd.id_hotel
                         join $bd.area a
                         on a.id_area = hd.id_area
                         join $bd.ciudad ciu 
                         on ciu.id_ciudad = ho.id_ciudad
                        WHERE id_hotel_$nombre != 0
                        AND hd.hd_estado = 0
                        AND id_cadena != 2
                        and hd.id_area in (".$this->info["area"].",3)
                        and ho.hot_estado = 0
                        and ho.hot_activo = 0
                        GROUP BY hm.id_pk
                        ORDER BY hot_nombre
                      ";

          }else{

          $consulta = "
                        SELECT 
                          hm.id_pk,
                          ho.hot_nombre,
                          MAX(hd.id_hotdet) as id_hotdet,
                          MAX(hd_fechasta) AS fecha,
                          area_nombre as id_area,
                          ciu_nombre
                        FROM
                          hoteles.hotelesmerge hm
                         JOIN $bd.hotdet hd
                         ON hd.id_hotel = hm.id_hotel_$nombre 
                         join $bd.hotel ho 
                         on ho.id_hotel = hd.id_hotel
                         join $bd.area a
                         on a.id_area = hd.id_area
                         join $bd.ciudad ciu 
                         on ciu.id_ciudad = ho.id_ciudad
                        WHERE id_hotel_$nombre != 0
                        AND hd.hd_estado = 0
                        AND id_cadena != 2
                        and hd.id_area = ".$this->info["area"]."
                        and ho.hot_estado = 0
                        and ho.hot_activo = 0
                        GROUP BY hm.id_pk
                        ORDER BY hot_nombre
                      ";


           }

            
            $traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);


            while(!$traer->EOF){  

                $id_pk = $traer->Fields("id_pk");
                $maxima = date("d-m-Y H:i:s",strtotime($traer->Fields("fecha")));
                $nombre_hotel = trim(utf8_encode($traer->Fields("hot_nombre")));
                $id_hotdet = $traer->Fields("id_hotdet");
                $area = $traer->Fields("id_area");
                $ciudad = trim(utf8_encode($traer->Fields("ciu_nombre")));

                
                 $estado = $this->comparar_fechas(date("Y-m-d",strtotime($traer->Fields("fecha"))));

                //echo $hoy."<=>".$comparar."<br>";

                $datos = array("id_pk"=>$id_pk,
                               "nombre_hotel"=>$nombre_hotel,
                               "maxima"=>$maxima,
                               "hotdet"=>$id_hotdet,
                               "estado"=>$estado,
                               "area"=>$area,
                               "ciudad"=>$ciudad
                               );
                array_push($this->datos["informacion"],$datos);

              $traer->MoveNext();
            }
    }


     protected function comparar_fechas($fecha){

        $hoy = date("Y-m-d");

        $fecha_inicio = new DateTime($hoy);
        $fecha_fin    = new DateTime($fecha);

        if($fecha_inicio > $fecha_fin)
          $estado = 1;
        else
          $estado = 0;

        return $estado ;

    }

	protected function buscar_bd(){

		$consulta = "select bd from hoteles.clientes where nombre ='".$this->session["cliente"]."'  ";
		$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

		$this->info["bd"] = trim($traer->Fields("bd"));

	} 


	protected function buscar_operador($id_operador){

		$consulta = "select hot_nombre from ".$this->info["bd"].".hotel where id_hotel = $id_operador ";
		$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

		return utf8_encode(trim($traer->Fields("hot_nombre")));

	} 


	protected function buscar_usuario_creador($id_usuario){

		$consulta = "select usu_login from ".$this->info["bd"].".usuarios where id_usuario = $id_usuario ";
		$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

		return utf8_encode(trim($traer->Fields("usu_login")));

	} 


	protected function buscar_nemo($nemo){

		$retornar = "";
		
		if($nemo != ""){
			$consulta = "select nemo from ".$this->info["bd"].".nemo where id_nemo = $nemo ";
			$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);
			$retornar = utf8_encode(trim($traer->Fields("nemo")));
		}

		return $retornar;
	} 

	protected function buscar_pk(){

		$retornar = "";
		
		
			$consulta = "select id_pk from hoteles.hotelesmerge where id_hotel_".$this->session["cliente"]." = ".$this->info["id_hotel"]." ";
			$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);
			
			if($traer->RecordCount() > 0)
				$retornar = $traer->Fields("id_pk");
		
		return $retornar;
	}


	protected function buscar_hotel(){

		$retornar = "";
		
			$consulta = "select id_hotel_".$this->session["cliente"]." as id_hotel from hoteles.hotelesmerge where id_pk = ".$this->info["hotel"]." ";
			$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

			if($traer->RecordCount() > 0)
				$retornar = $traer->Fields("id_hotel");
	
		return $retornar;
	} 


	protected function errores($linea){
		die($_SERVER['REQUEST_URI']." - ".$linea." : ".$this->sql_con->ErrorMsg());
	}


	public function __destruct(){
		$this->sql_con->close();
		echo json_encode($this->datos);
	}

}