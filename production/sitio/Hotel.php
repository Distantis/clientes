<?php

$hotel = new Hotel();

class Hotel{
	protected $sql_con;
	protected $datos = array();
	protected $info = array();
	protected $session = array();

	public function __construct(){
		
		error_reporting(0);
		session_start();
		require_once('/var/www/h2o/Connections/db1.php');
		$this->conectar($db1);
		$this->obtener_info();
	}

	protected function conectar($db1){
		$this->sql_con = $db1;
	}

	protected function obtener_info(){

		extract($_POST);

		foreach ($_SESSION as $key => $value) {
			$this->session["".$key.""] = $value;
		}

		foreach ($_POST as $key => $value) {
			$this->info["".$key.""] = $value;
		}


		switch ($this->info["tipo"]) {
			case 1:
				$this->buscar_hotel();	
			break;

			case 2:
				$this->buscar_bd();
				$this->buscar_ciudad();	
			break;

			case 3:
				$this->buscar_bd();
				$this->buscar_operador();	
			break;

			case 4:
				$this->buscar_bd();
				$this->buscar_cliente();	
			break;

			case 5:	// SOLO COCHA
				$this->buscar_nemo();	
			break;

			case 6:
				$this->buscar_bd();
				$this->buscar_pais();	
			break;

			case 7:
				$this->buscar_bd();
				$this->buscar_hotel_operador();	
			break;
			
			case 8:
				$this->buscar_bd();
				$this->buscar_dispo_global();	
			break;


		}


	}


	protected function buscar_dispo_global(){

		if($this->info["desde"] == "")
			$this->info["desde"] = date('Y-m-d');
		else
			$this->info["desde"] = date("Y-m-d",strtotime($this->info["desde"]));


		if($this->info["hasta"] == "")
			$this->info["hasta"] = date("Y-m-d" ,strtotime("+ 10 days", strtotime($this->info["desde"])));
		else
			$this->info["hasta"] = date("Y-m-d",strtotime($this->info["hasta"]));


		if($this->info["ciudad"] == "")
			$this->info["ciudad"] = 96;

		
		$this->datos["dias"] = array();
		$this->datos["fecha_inicio"] = date("d-m-Y",strtotime($this->info["desde"]));
		$this->datos["fecha_fin"] = date("d-m-Y",strtotime($this->info["hasta"]));

		$cont = 0;
		$this->info["buscar"] = 0;
		for($i=$this->info["desde"];$i<=$this->info["hasta"];$i = date("Y-m-d", strtotime($i ."+ 1 days"))){
			$dato = array("dias"=>date("d-m-Y",strtotime($i)));
			array_push($this->datos["dias"],$dato);

			if($this->info["disponibilidad"] == 0)
				$this->traer_dispo_por_dia_global($i);
			else
				$this->traer_dispo_por_dia_normal($i);

			$cont+=1;
		}


	}


	protected function traer_dispo_por_dia_normal($fecha){


		$consulta = "	SELECT s.id_hotdet,s.sc_fecha,s.sc_cerrado,hot_nombre,th.id_tipohabitacion,th_nombre FROM 
							".$this->info["bd"].".stock s 
							JOIN ".$this->info["bd"].".hotdet hd 
								ON hd.id_hotdet = s.id_hotdet 
							JOIN ".$this->info["bd"].".hotel ho 
								ON ho.id_hotel = hd.id_hotel
							JOIN ".$this->info["bd"].".tipohabitacion th 
								ON th.id_tipohabitacion = hd.id_tipohabitacion
							JOIN hoteles.hotelesmerge hm 
								ON hm.id_hotel_".$this->info["operador"]." = ho.id_hotel
							WHERE sc_fecha = '$fecha' 
							and ho.id_ciudad = ".$this->info["ciudad"]."
							AND hd.hd_estado = 0 
							AND hd.hd_fechasta>='$fecha'
							AND hm.ver = 1
					";

		$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

		$this->datos[date("d-m-Y",strtotime($fecha))] = array();

		
		while(!$traer->EOF){
			$dia = date('d-m-Y', strtotime($traer->Fields("sc_fecha")));
		  	$fecha = $traer->Fields("sc_fecha");
            $hotdet = $traer->Fields("id_hotdet");
            $hab = $traer->Fields("id_tipohabitacion");
			$th_nombre = $traer->Fields("th_nombre");



            $stock_sgl = $this->stock_normal(1,$fecha,$hotdet);
            $stock_twn = $this->stock_normal(2,$fecha,$hotdet);
            $stock_dbl = $this->stock_normal(3,$fecha,$hotdet);
            $stock_tpl = $this->stock_normal(4,$fecha,$hotdet);


            $hotocu_sgl = $this->hotocu_normal(1,$hotdet,$fecha);
            $hotocu_twn = $this->hotocu_normal(2,$hotdet,$fecha);
            $hotocu_dbl = $this->hotocu_normal(3,$hotdet,$fecha);
            $hotocu_tpl = $this->hotocu_normal(4,$hotdet,$fecha);


            $dispo_normal_sgl = $stock_sgl*1 - $hotocu_sgl*1;
            $dispo_normal_twn = $stock_twn*1 - $hotocu_twn*1;
            $dispo_normal_dbl = $stock_dbl*1 - $hotocu_dbl*1;
            $dispo_normal_tpl = $stock_tpl*1 - $hotocu_tpl*1;



            if($dispo_normal_sgl<0)
                $dispo_normal_sgl = 0;

            if($dispo_normal_twn<0)
                $dispo_normal_twn = 0;

            if($dispo_normal_dbl<0)
                $dispo_normal_dbl = 0;

            if($dispo_normal_tpl<0)
                $dispo_normal_tpl = 0;

	      

		  $nombre_hotel = trim(utf8_encode($traer->Fields("hot_nombre")));
		  $cerrado = $traer->Fields("sc_cerrado");


		  $dispo_futura = 0;
		  
		  //if($traer->Fields("sc_fecha") == $this->info["desde"])
		  		//$dispo_futura = $this->buscar_dipo_futura($pk,$hab,$modo);
		    

		  	if($nombre_hotel != ""){

				  $dato = array("dia"=>$dia,
				  				"habitacion"=>$hab,
				  				"th_nombre"=>$th_nombre,
				  				"nombre_hotel" => $nombre_hotel,
				  				"sgl"=>$dispo_normal_sgl,
				  				"twn"=>$dispo_normal_twn,
				  				"dbl"=>$dispo_normal_dbl,
				  				"tpl"=>$dispo_normal_tpl,
				  				"cerrado"=>$cerrado,
				  				"modo"=>0,
				  				"dispo_futura"=>$dispo_futura
				  				);

				  array_push($this->datos[date("d-m-Y",strtotime($fecha))], $dato);

			}

		  $traer->MoveNext();
		}


}


	protected function traer_dispo_por_dia_global($fecha){


		$consulta = "select * from hoteles.stock_global sg 
						left join hoteles.hotdet hd on hd.id_hotdet = sg.id_hotdet
						left join hoteles.tipohabitacion th on th.id_tipohabitacion = hd.id_tipohabitacion 
						left join hoteles.hotelesmerge hm on hm.id_pk = hd.id_pk
						join ".$this->info["bd"].".hotel h on h.id_hotel = hm.id_hotel_".$this->info["operador"]."
						where hd.hd_estado = 0 
						and sc_fecha  = '".$fecha."' 
						and hm.id_hotel_".$this->info["operador"]." != 0
						and hm.ver = 0
						and h.id_ciudad = ".$this->info["ciudad"]."
						and hm.id_cadena not in (2,1092)
						order by sg.id_pk,id_tipohab,sc_fecha";

		$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

		$this->datos[date("d-m-Y",strtotime($fecha))] = array();

		
		while(!$traer->EOF){

		  $dia = date('d-m-Y', strtotime($traer->Fields("sc_fecha")));
		  $hab = $traer->Fields("id_tipohab");
		  $pk = $traer->Fields("id_pk");
		  $id_stock_global = $traer->Fields("id_stock_global");
		  $th_nombre = $traer->Fields("th_nombre");


		  $stock_global_sgl = $traer->Fields("sc_hab1");
       	  $stock_global_twn = $traer->Fields("sc_hab2");
          $stock_global_dbl = $traer->Fields("sc_hab3");
          $stock_global_tpl = $traer->Fields("sc_hab4");

          $hotocu_global_sgl = $this->hotocu_global(1,$traer->Fields("sc_fecha"),$id_stock_global,$hab);
          $hotocu_global_twn = $this->hotocu_global(2,$traer->Fields("sc_fecha"),$id_stock_global,$hab);
          $hotocu_global_dbl = $this->hotocu_global(3,$traer->Fields("sc_fecha"),$id_stock_global,$hab);
          $hotocu_global_tpl = $this->hotocu_global(4,$traer->Fields("sc_fecha"),$id_stock_global,$hab);

	      $sgl = $stock_global_sgl*1 - $hotocu_global_sgl*1;
	      $twn = $stock_global_twn*1 - $hotocu_global_twn*1;
	      $dbl = $stock_global_dbl*1 - $hotocu_global_dbl*1;
	      $tpl = $stock_global_tpl*1 - $hotocu_global_tpl*1;


	      if($sgl < 0)
	      	  $sgl = 0;


	      if($twn < 0)
	      	  $twn = 0;


	      if($dbl < 0)
	      	  $dbl = 0;


	      if($tpl < 0)
	      	  $tpl = 0;

	      

		  $nombre_hotel = trim(utf8_encode($traer->Fields("nombre")));
		  $cerrado = $traer->Fields("sc_cerrado");
		  $modo = $this->buscar_modo($pk);
		  

		  $dispo_futura = 0;
		  
		  if($traer->Fields("sc_fecha") == $this->info["desde"])
		  		$dispo_futura = $this->buscar_dipo_futura($pk,$hab,$modo);
		    

		  	if($nombre_hotel != ""){

				  $dato = array("dia"=>$dia,
				  				"habitacion"=>$hab,
				  				"th_nombre"=>$th_nombre,
				  				"nombre_hotel" => $nombre_hotel,
				  				"id_pk"=>$pk,
				  				"sgl"=>$sgl,
				  				"twn"=>$twn,
				  				"dbl"=>$dbl,
				  				"tpl"=>$tpl,
				  				"cerrado"=>$cerrado,
				  				"modo" =>$modo,
				  				"dispo_futura"=>$dispo_futura
				  				);

				  array_push($this->datos[date("d-m-Y",strtotime($fecha))], $dato);

			}

		  $traer->MoveNext();
		}


}

	protected function buscar_hotel(){

		$consulta = "select id_pk,nombre,id_hotel_".$this->session["cliente"]." as hotel from hoteles.hotelesmerge where id_hotel_".$this->session["cliente"]." != 0 and nombre <> '0' and nombre is not null and nombre <> 1  order by nombre";
		$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

		$this->datos["hoteles"] = array();

		while(!$traer->EOF){	

			$pk = $traer->Fields("id_pk");
			$nombre = $traer->Fields("nombre");
			$hotel = $traer->Fields("hotel");

			$datos = array(
							"pk"=>$pk,
							"nombre_hotel"=>$nombre,
							"hotel"=>$hotel
						);

			array_push($this->datos["hoteles"],$datos);

			$traer->MoveNext();
		}


	}

	protected function buscar_ciudad(){

		$consulta = "SELECT * FROM ".$this->info["bd"].".ciudad WHERE ciu_estado = 0 ORDER BY ciu_nombre";
		$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

		$this->datos["ciudades"] = array();

		while(!$traer->EOF){	

			$id_ciudad = $traer->Fields("id_ciudad");
			$nombre = utf8_encode(trim($traer->Fields("ciu_nombre")));
			
			$datos = array(
							"id_ciudad"=>$id_ciudad,
							"nombre_ciudad"=>$nombre
						);

			array_push($this->datos["ciudades"],$datos);

			$traer->MoveNext();
		}


	}


	protected function buscar_operador(){

		$consulta = "SELECT * FROM ".$this->info["bd"].".hotel WHERE hot_estado = 0 AND id_tipousuario = 3 AND hot_activo = 0 ORDER BY hot_nombre";
		$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

		$this->datos["operadores"] = array();

		while(!$traer->EOF){	

			$id_operador = $traer->Fields("id_hotel");
			$nombre_hot = utf8_encode($traer->Fields("hot_nombre"));
			
			$datos = array(
							"id_operador"=>$id_operador,
							"nombre_hot"=>$nombre_hot
						);

			array_push($this->datos["operadores"],$datos);

			$traer->MoveNext();
		}


	}



	protected function buscar_cliente(){

		$consulta = "SELECT DISTINCT id_opcts FROM ".$this->info["bd"].".cot WHERE id_opcts IS NOT NULL order by id_opcts";
		$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);


		$this->datos["clientes"] = array();

		while(!$traer->EOF){	

			$id_opcts = utf8_encode($traer->Fields("id_opcts"));
			
			$datos = array(
							"id_opcts"=>$id_opcts
						);

			array_push($this->datos["clientes"],$datos);

			$traer->MoveNext();
		}	


	}


	protected function buscar_pais(){

		$consulta = "SELECT * FROM ".$this->info["bd"].".pais WHERE pai_estado = 0 ORDER BY pai_nombre";
		$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

		$this->datos["paises"] = array();

		while(!$traer->EOF){	

			$id_pais = $traer->Fields("id_pais");
			$nombre = utf8_encode(trim($traer->Fields("pai_nombre")));
			
			$datos = array(
							"id_pais"=>$id_pais,
							"nombre_pais"=>$nombre
						);

			array_push($this->datos["paises"],$datos);

			$traer->MoveNext();
		}


	}


	protected function buscar_hotel_operador(){

		$buscar = "";

		if($this->info["nombre_hotel"] != "")
			$buscar.=" and hot_nombre like  '%".$this->info["nombre_hotel"]."%' ";

		if($this->info["id_hotel"] != "")
			$buscar.=" and id_hotel =  ".$this->info["id_hotel"]." ";

		if($this->info["pais"] != "")
			$buscar.=" and ciu.id_pais =  ".$this->info["pais"]." ";

		if($this->info["ciudad"] != "")
			$buscar.=" and ciu.id_ciudad =  ".$this->info["ciudad"]." ";

		if($this->info["hoteles"] != ""){
			$hotel = $this->buscar_id_hotel();
			$buscar.=" and ho.id_hotel =  ".$hotel." ";
		}

		if($this->info["estado"] != "")
			$buscar.=" and hot_estado =  ".$this->info["estado"]." ";



		$consulta = "select * from ".$this->info["bd"].".hotel ho 
						left join ".$this->info["bd"].".ciudad ciu on ciu.id_ciudad = ho.id_ciudad 
						left join ".$this->info["bd"].".comuna co on co.id_comuna = ho.id_comuna
						where habxvender = 0 $buscar order by ho.hot_nombre";

		$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);


		$this->datos["hoteles_operador"] = array();

		while(!$traer->EOF){	

			$id_hotel = $traer->Fields("id_hotel");
			$nombre_hotel = utf8_encode(trim($traer->Fields("hot_nombre")));
			$direccion = utf8_encode(trim($traer->Fields("hot_direccion")));
			$pais = utf8_encode(trim($traer->Fields("pai_nombre")));
			$comuna = utf8_encode(trim($traer->Fields("com_nombre")));
			$ciudad = utf8_encode(trim($traer->Fields("ciu_nombre")));
			$fono = $traer->Fields("hot_fono");
			$email = utf8_encode(trim($traer->Fields("hot_email")));
			$estado = $traer->Fields("hot_estado");
			
			$datos = array(
							"id_hotel"=>$id_hotel,
							"nombre_hotel"=>$nombre_hotel,
							"direccion"=>$direccion,
							"pais"=>$pais,
							"comuna"=>$comuna,
							"ciudad"=>$ciudad,
							"fono"=>$fono,
							"email"=>$email,
							"estado"=>$estado,
							"id_pk"=>$this->buscar_pk($id_hotel)
						);

			array_push($this->datos["hoteles_operador"],$datos);

			$traer->MoveNext();
		}	


	}


	protected function buscar_dipo_futura($pk,$hab,$modo){

		if($modo == 0){

			$consulta_stock = "
								SELECT SUM(sc_hab1+sc_hab2+sc_hab3+sc_hab4) as total_stock,
								MIN(id_stock_global) as minimo_stock_global,
								MAX(id_stock_global) as maximo_stock_global
								FROM hoteles.`stock_global` 
								WHERE id_pk = $pk 
								AND id_tipohab = $hab 
								AND sc_fecha>=NOW()
								AND sc_estado = 0

							";

		    $traer = $this->sql_con->SelectLimit($consulta_stock) or $this->errores(__LINE__);

		    $minimo = $traer->Fields("minimo_stock_global");
		    $maximo = $traer->Fields("maximo_stock_global");

		    $consulta_ocupacion = "
								SELECT SUM(hc_hab1+hc_hab2+hc_hab3+hc_hab4) as total_ocupacion
								FROM hoteles.hotocu
								WHERE id_stock_global BETWEEN '$minimo' AND '$maximo' 
								AND hc_estado = 0

							";

			$traer_ocupacion = $this->sql_con->SelectLimit($consulta_ocupacion) or $this->errores(__LINE__);

			$stock_total = $traer->Fields("total_stock");
			$ocupacion_total = $traer->Fields("total_ocupacion");		


			$retornar = $stock_total - $ocupacion_total;		


		}else{

			$consulta_stock = "
								SELECT SUM(sc_hab1) as total_stock,
								MIN(id_stock_global) as minimo_stock_global,
								MAX(id_stock_global) as maximo_stock_global
								FROM hoteles.`stock_global` 
								WHERE id_pk = $pk 
								AND id_tipohab = $hab 
								AND sc_fecha>=NOW()
								AND sc_estado = 0

							";

		    $traer = $this->sql_con->SelectLimit($consulta_stock) or $this->errores(__LINE__);

		    $minimo = $traer->Fields("minimo_stock_global");
		    $maximo = $traer->Fields("maximo_stock_global");

		    $consulta_ocupacion = "
								SELECT SUM(hc_hab1) as total_ocupacion
								FROM hoteles.hotocu
								WHERE id_stock_global BETWEEN '$minimo' AND '$maximo' 
								AND hc_estado = 0

							";

			$traer_ocupacion = $this->sql_con->SelectLimit($consulta_ocupacion) or $this->errores(__LINE__);

			$stock_total = $traer->Fields("total_stock");
			$ocupacion_total = $traer->Fields("total_ocupacion");		


			$retornar = $stock_total - $ocupacion_total;			

		}

		return $retornar;
	}


	protected function stock_global($tipo,$fecha,$pk,$habitacion){

        switch ($tipo) {

                case 1:
                  
                   $consulta = "select sum(sc_hab1) as valor from hoteles.stock_global where sc_fecha = '$fecha' and id_pk = $pk and id_tipohab = $habitacion";
      
                break;

                case 2:
                  
                   $consulta = "select sum(sc_hab2) as valor from hoteles.stock_global where sc_fecha = '$fecha' and id_pk = $pk and id_tipohab = $habitacion";

                break;

                case 3:
                  
                   $consulta = "select sum(sc_hab3) as valor from hoteles.stock_global where sc_fecha = '$fecha' and id_pk = $pk and id_tipohab = $habitacion";

                break;

                case 4:
                  
                   $consulta = "select sum(sc_hab4) as valor from hoteles.stock_global where sc_fecha = '$fecha' and id_pk = $pk and id_tipohab = $habitacion";

                break;
            
            
        }


        $traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

        return $traer->Fields("valor");

    }


    protected function hotocu_global($tipo,$fecha,$id_stock_global,$habitacion){

        switch ($tipo) {

                case 1:
                  
                   $consulta = "select sum(hc_hab1) as valor from hoteles.hotocu where hc_fecha = '$fecha' and id_stock_global = $id_stock_global and hc_estado = 0";

                break;

                case 2:
                  
                   $consulta = "select sum(hc_hab2) as valor from hoteles.hotocu where hc_fecha = '$fecha' and id_stock_global = $id_stock_global and hc_estado = 0";

                break;

                case 3:
                  
                   $consulta = "select sum(hc_hab3) as valor from hoteles.hotocu where hc_fecha = '$fecha' and id_stock_global = $id_stock_global and hc_estado = 0";

                break;

                case 4:
                  
                   $consulta = "select sum(hc_hab4) as valor from hoteles.hotocu where hc_fecha = '$fecha' and id_stock_global = $id_stock_global and hc_estado = 0";

                break;
            
            
        }

        $traer = $this->sql_con->SelectLimit($consulta)  or $this->errores(__LINE__);

        $valor = $traer->Fields("valor");

        if($valor < 0)
            $valor = 0;


        return $valor;

    }



    protected function stock_normal($tipo,$fecha,$hotdet){

        switch ($tipo) {

                case 1:
                  
                   $consulta = "select sum(sc_hab1) as valor from ".$this->info["bd"].".stock where sc_fecha = '$fecha' and id_hotdet = $hotdet and sc_estado = 0";

                break;

                case 2:
                  
                   $consulta = "select sum(sc_hab2) as valor from ".$this->info["bd"].".stock where sc_fecha = '$fecha' and id_hotdet = $hotdet and sc_estado = 0";

                break;

                case 3:
                  
                   $consulta = "select sum(sc_hab3) as valor from ".$this->info["bd"].".stock where sc_fecha = '$fecha' and id_hotdet = $hotdet and sc_estado = 0";

                break;

                case 4:
                  
                   $consulta = "select sum(sc_hab4) as valor from ".$this->info["bd"].".stock where sc_fecha = '$fecha' and id_hotdet = $hotdet and sc_estado = 0";

                break;
            
            
        }
      
        $traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

        return $traer->Fields("valor");

    }


    protected function hotocu_normal($tipo,$hotdet,$fecha){

        switch ($tipo) {

                case 1:
                  
                   $consulta = "select sum(hc_hab1) as valor from ".$this->info["bd"].".hotocu where hc_fecha = '$fecha' and id_hotdet = $hotdet and hc_estado = 0";

                break;

                case 2:
                  
                   $consulta = "select sum(hc_hab2) as valor from ".$this->info["bd"].".hotocu where hc_fecha = '$fecha' and id_hotdet = $hotdet and hc_estado = 0";

                break;

                case 3:
                  
                   $consulta = "select sum(hc_hab3) as valor from ".$this->info["bd"].".hotocu where hc_fecha = '$fecha' and id_hotdet = $hotdet and hc_estado = 0";

                break;

                case 4:
                  
                   $consulta = "select sum(hc_hab4) as valor from ".$this->info["bd"].".hotocu where hc_fecha = '$fecha' and id_hotdet = $hotdet and hc_estado = 0";

                break;
            
            
        }

        $traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

        $valor = $traer->Fields("valor");

        if($valor < 0)
            $valor = 0;


        return $valor;

    }


	protected function buscar_id_hotel(){

		$retornar = "";
		
			$consulta = "select id_hotel_".$this->session["cliente"]." as id_hotel from hoteles.hotelesmerge where id_pk = ".$this->info["hoteles"]." ";
			$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

			if($traer->RecordCount() > 0)
				$retornar = $traer->Fields("id_hotel");
	
		return $retornar;
	} 



	// SOLO COCHA
	protected function buscar_nemo(){

		$consulta = "select * from cocha.nemo order by nemo";
		$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);


		$this->datos["nemo"] = array();

		while(!$traer->EOF){	

			$id_nemo = $traer->Fields("nemo");
			$nemo = utf8_encode(trim($traer->Fields("nemo")));
			$r_social = utf8_encode(trim($traer->Fields("razon_social")));
			
			$datos = array(
							"id_nemo"=>$id_nemo,
							"nemo"=>$nemo,
							"r_social"=>$r_social
						);

			array_push($this->datos["nemo"],$datos);

			$traer->MoveNext();
		}	


	}



	protected function buscar_bd(){

		$consulta = "select bd from hoteles.clientes where nombre ='".$this->session["cliente"]."'  ";
		$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

		$this->info["bd"] = trim($traer->Fields("bd"));

	}


	protected function buscar_pk($id_hotel){

		$retornar = "";
		
			$consulta = "select id_pk from hoteles.hotelesmerge where id_hotel_".$this->session["cliente"]." = ".$id_hotel." ";
			$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);
			
			if($traer->RecordCount() > 0)
				$retornar = $traer->Fields("id_pk");
		
		return $retornar;
	}


	protected function buscar_modo($pk){

		$consulta = "select modo from hoteles.hotelesmerge hm join hoteles.cadena c on c.id_cadena = hm.id_cadena where id_pk = $pk ";
		$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

		return $traer->Fields("modo");

	}


	protected function errores($linea){
		die($_SERVER['REQUEST_URI']." - ".$linea." : ".$this->sql_con->ErrorMsg());
	}


	public function __destruct(){
		$this->sql_con->close();
		echo json_encode($this->datos);
	}

}