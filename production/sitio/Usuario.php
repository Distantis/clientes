<?php

$usuario = new usuario();

class Usuario{
	protected $sql_con;
	protected $datos = array();
	protected $info = array();
	protected $session = array();

	public function __construct(){
		session_start();
		error_reporting(0);
		require_once('/var/www/h2o/Connections/db1.php');
		$this->conectar($db1);
		$this->obtener_info();
	}

	protected function conectar($db1){
		$this->sql_con = $db1;
	}

	protected function obtener_info(){

		extract($_POST);

		foreach ($_SESSION as $key => $value) {
			$this->session["".$key.""] = $value;
		}

		foreach ($_POST as $key => $value) {
			$this->info["".$key.""] = $value;
		}

		$this->buscar_bd();


		switch ($this->info["tipo"]) {
			case 1:
				$this->buscar_creador();	
			break;

		}


	}

	protected function buscar_creador(){

		$consulta = " SELECT id_usuario,usu_login FROM ".$this->info["bd"].".usuarios u INNER JOIN ".$this->info["bd"].".hotel h ON u.id_empresa = h.id_hotel WHERE u.usu_estado = 0 ORDER BY u.usu_login, h.hot_nombre";
		$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

		$this->datos["creador"] = array();

		while(!$traer->EOF){	

			$id_usuario = $traer->Fields("id_usuario");
			$usu_login = $traer->Fields("usu_login");
			
			$datos = array(
							"id_usuario"=>$id_usuario,
							"usu_login"=>trim(utf8_encode($usu_login))
						);

			array_push($this->datos["creador"],$datos);

			$traer->MoveNext();
		}


	}

	protected function buscar_bd(){

		$consulta = "select bd from hoteles.clientes where nombre ='".$this->session["cliente"]."'  ";
		$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

		$this->info["bd"] = trim($traer->Fields("bd"));

	}

	protected function errores($linea){
		die($_SERVER['REQUEST_URI']." - ".$linea." : ".$this->sql_con->ErrorMsg());
	}


	public function __destruct(){
		$this->sql_con->close();
		echo json_encode($this->datos);
	}

}