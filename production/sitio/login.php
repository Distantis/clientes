<?php

$login = new Login();

class Login{
	protected $sql_con;
	protected $datos = array();
	protected $info = array();

	public function __construct(){
		session_start();
		error_reporting(0);
		
		if(empty($_SESSION["cliente"]))
			$this->datos["cliente"] = strtolower(str_replace(".distantis.com","",$_SERVER["SERVER_NAME"]));

		/*if(is_numeric($this->info["cliente"])) //cambiar cuando este el nuevo dominio
			$this->info["cliente"] = $_SESSION["cliente"];*/

		require_once('/var/www/h2o/Connections/db1.php');
		$this->conectar($db1);
		$this->obtener_info();
	}

	protected function conectar($db1){
		$this->sql_con = $db1;
	}

	protected function obtener_info(){

		extract($_POST);

		foreach ($_POST as $key => $value) {
			$this->info["".$key.""] = $value;
		}

		switch ($this->info["tipo"]) {
			case 1:
				$this->verificar();	
			break;

			case 2:
				$this->verificar_usuario();	
			break;

			case 3:
				$this->cerrar_session();	
			break;

		}

	}


	protected function verificar(){

    	if(isset($_SESSION["usuario"])){
    		$this->datos["respuesta"] = 1;
    		$this->datos["usuario"]  = trim($_SESSION['usuario']);
    		$this->datos["id_usuario"]  = trim($_SESSION['id_usuario']);
    		$this->datos["pk"]  = trim($_SESSION['pk']);
    		$this->datos["hotel"]  = trim($_SESSION['hotel']);
    		$this->datos["cliente"]  = trim($_SESSION['cliente']);
    	}else
    		$this->datos["respuesta"] = 0;

    }

    protected function verificar_usuario(){

    	$cont = 0;

    	$bd = $this->info["cliente"];
    	if($bd == "turavion") $bd = "touravion_dev"; // sacar después

    	$consulta = "select * from ".$bd.".usuarios where usu_login='".$this->info["usuario"]."' and usu_password='".$this->info["pass"]."' and usu_estado = 0 ";
    	$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

    	if($traer->RecordCount() > 0){
    		$_SESSION['usuario']=$traer->Fields("usu_nombre")." ".$traer->Fields("usu_pat")." ".$traer->Fields("usu_mat");
    		$_SESSION['id_usuario']=$traer->Fields("id_usuario");
    		$_SESSION['hotel']=$traer->Fields("id_empresa");
    		$_SESSION['pk']=$this->buscar_pk($traer->Fields("id_empresa"));
    		$_SESSION['cliente'] = $this->info["cliente"];
    		$this->dato["cliente"] = $this->info["cliente"];
    		$cont+=1;
    	}
    	

    	if($cont > 0)
    		$this->datos["respuesta"] = 1;
    	else
    		$this->datos["respuesta"] = 0;

    }


    protected function buscar_pk($hotel){

    	$consulta = "select id_pk from hoteles.hotelesmerge where id_hotel_".$this->info["cliente"]." = $hotel ";
    	$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

    	return $traer->Fields("pk");

    }

    protected function cerrar_session(){

    	session_destroy();
    	$this->datos["respuesta"] = 1;

    }

	protected function errores($linea){
		die($_SERVER['REQUEST_URI']." - ".$linea." : ".$this->sql_con->ErrorMsg());
	}

	public function __destruct(){
		$this->sql_con->close();
		echo json_encode($this->datos);
	}

}



?>