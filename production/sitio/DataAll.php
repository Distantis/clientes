<?php

$dataall = new DataALL();

class DataALL{
	protected $sql_con;
	protected $datos = array();
	protected $info = array();
	protected $session = array();

	public function __construct(){
		error_reporting(0);
		session_start();
		require_once('/var/www/h2o/Connections/db1.php');
		$this->conectar($db1);
		$this->obtener_info();
	}

	protected function conectar($db1){
		$this->sql_con = $db1;
	}

	protected function obtener_info(){

		extract($_POST);

		foreach ($_SESSION as $key => $value) {
			$this->session["".$key.""] = $value;
		}

		foreach ($_POST as $key => $value) {

			if($key == "desde" or $key == "hasta" and ($value!=""))
				$value = date("Y-m-d", strtotime($value));

			$this->info["".$key.""] = $value;
		}

		$this->buscar_bd();


		switch ($this->info["tipo"]) {
			case 1:
				$this->buscar_bd();
				$this->agregar_hotdet_clientes();	
			break;

		}
	}


	protected function agregar_hotdet_clientes(){


		foreach($this->info["informacion"] as $x=>$informacion){
			foreach ($informacion as $key => $value) {

				if($key == "desde" or $key == "hasta")
					$value = date("Y-m-d",strtotime($value));
				
				$this->info[$key]=$value;
			}
        }

        $this->info["dias"] = array();
        foreach($this->info["dias_cerrar"] as $key=>$informacion){
            $this->info["dias"][] = $informacion["dias"];

        }


        $this->traer_hotel();

        $fecha = date('Y-m-d');

        if($this->info["hasta"] < $fecha){
            $this->datos["respuesta"] = 2;
        }else{

        	$revisar = $this->revisar_hab_global_porcliente();
        	if($revisar == 0)
        		$this->datos["respuesta"] = 3;
        	else{

        		if($this->info["editar"] == 0)
					$this->agregar_hotdet();
				else
					$this->actualizar_hotdet();
        	}
        }
        
     
	}


	protected function actualizar_hotdet(){

	   $hotdet = "";

	   if($this->info["bd"] == "touravion_dev"){

	   	   $hotdet = "
	   	   				update ".$this->info["bd"].".hotdet set 
		   	   				id_hotel=".$this->info["hotel"].",
		   	   				hd_fecdesde='".$this->info["desde"]."',
		   	   				hd_fechasta='".$this->info["hasta"]."',
		   	   				hd_sgl=".$this->info["p_sgl"].",
		   	   				hd_dbl=".$this->info["p_dbl"].",
		   	   				hd_tpl=".$this->info["p_tpl"].",
		   	   				hd_mon=".$this->info["moneda"].",
		   	   				id_tipotarifa=".$this->info["tarifa"].",
		   	   				id_tipohabitacion=".$this->info["habitacion"].",
		   	   				hd_diadesde=".$this->info["dia_desde"].",
		   	   				hd_diahasta=".$this->info["dia_hasta"].",
		   	   				id_area=".$this->info["area"].",
		   	   				hd_sgl_vta=".$this->info["p_sgl"].",
		   	   				hd_dbl_vta=".$this->info["p_dbl"].",
		   	   				hd_tpl_vta=".$this->info["p_tpl"]."

		   	   			where id_hotdet = ".$this->info["hotdet"]."	
	   	   			";
	   }else{

	   		$hotdet = "
	   	   				update ".$this->info["bd"].".hotdet set 
		   	   				id_hotel=".$this->info["hotel"].",
		   	   				hd_fecdesde='".$this->info["desde"]."',
		   	   				hd_fechasta='".$this->info["hasta"]."',
		   	   				hd_sgl=".$this->info["p_sgl"].",
		   	   				hd_dbl=".$this->info["p_dbl"].",
		   	   				hd_tpl=".$this->info["p_tpl"].",
		   	   				hd_mon=".$this->info["moneda"].",
		   	   				id_tipotarifa=".$this->info["tarifa"].",
		   	   				id_tipohabitacion=".$this->info["habitacion"].",
		   	   				hd_diadesde=".$this->info["dia_desde"].",
		   	   				hd_diahasta=".$this->info["dia_hasta"].",
		   	   				id_area=".$this->info["area"]."

		   	   			where id_hotdet = ".$this->info["hotdet"]."	
	   	   			";

	   }


	   $actualizar = $this->sql_con->Execute($hotdet) or $this->errores(__LINE__);

		if($actualizar){

			$this->anular_stock();
			$this->crear_stock();

			if($this->info["cont_stock"] == $this->info["cont_stock_guardar"])
				$this->datos["respuesta"] = 4;
			else
				$this->datos["respuesta"] = 0;

		}else
			$this->datos["respuesta"] = 0;


	}


	protected function agregar_hotdet(){

		$estado = 1;
		$hotdet = "";

		if($this->info["bd"] == "touravion_dev"){

			$hotdet = "insert into ".$this->info["bd"].".hotdet (id_hotel,hd_fecdesde,hd_fechasta,hd_sgl,hd_dbl,hd_tpl,hd_mon,hd_estado,id_tipotarifa,id_tipohabitacion,hd_diadesde,hd_diahasta,id_area,hd_sgl_vta,hd_dbl_vta,hd_tpl_vta)
		                		values('".$this->info["hotel"]."','".$this->info["desde"]."','".$this->info["hasta"]."',
		                       '".$this->info["p_sgl"]."','".$this->info["p_dbl"]."','".$this->info["p_tpl"]."',
		                       '".$this->info["moneda"]."','".$estado."','".$this->info["tarifa"]."','".$this->info["habitacion"]."',
		                       '".$this->info["dia_desde"]."','".$this->info["dia_hasta"]."','".$this->info["area"]."',
		                       '".$this->info["p_sgl"]."','".$this->info["p_dbl"]."','".$this->info["p_tpl"]."')";

		}else{

			$hotdet = "insert into ".$this->info["bd"].".hotdet (id_hotel,hd_fecdesde,hd_fechasta,hd_sgl,hd_dbl,hd_tpl,hd_mon,hd_estado,id_tipotarifa,id_tipohabitacion,hd_diadesde,hd_diahasta,id_area)
		                values('".$this->info["hotel"]."','".$this->info["desde"]."','".$this->info["hasta"]."',
		                       '".$this->info["p_sgl"]."','".$this->info["p_dbl"]."','".$this->info["p_tpl"]."',
		                       '".$this->info["moneda"]."','".$estado."','".$this->info["tarifa"]."','".$this->info["habitacion"]."',
		                       '".$this->info["dia_desde"]."','".$this->info["dia_hasta"]."','".$this->info["area"]."')";

			
		}


		$guardar = $this->sql_con->Execute($hotdet) or $this->errores(__LINE__);

		if($guardar){

			$this->buscar_hotdet();
			$this->crear_stock();

			if($this->info["cont_stock"] == $this->info["cont_stock_guardar"])
				$this->datos["respuesta"] = 1;
			else
				$this->datos["respuesta"] = 0;

		}else
			$this->datos["respuesta"] = 0;

	}


	protected function anular_stock(){

		$act = "update ".$this->info["bd"].".stock set sc_estado = 1 where id_hotdet = ".$this->info["hotdet"]." ";
		$actualizando = $this->sql_con->Execute($act) or $this->errores(__LINE__);

	}

	protected function crear_stock(){

		$this->info["cont_stock"] = 0;
		$this->info["cont_stock_guardar"] = 0;
        for($i=''.$this->info["desde"].'';$i<=''.$this->info["hasta"].'';$i = date("Y-m-d", strtotime($i ."+ 1 days"))){
        	$this->info["cont_stock"]+=1;
            $dia = date('w',strtotime($i));

            if(in_array($dia, $this->info["dias"]))
                $sc_estado = 0;
            else
                $sc_estado = 1;
            
            $insertar = "INSERT INTO ".$this->info["bd"].".stock (id_hotdet,sc_fecha,sc_hab1,sc_hab2,sc_hab3,sc_hab4,sc_estado,sc_minnoche,sc_cerrado)
                         VALUES ('".$this->info["hotdet"]."','$i',0,0,0,0,$sc_estado,".$this->info["min_noche"].",0)";

            $guardar = $this->sql_con->Execute($insertar) or $this->errores(__LINE__);

            if($guardar) 
            	$this->info["cont_stock_guardar"]+=1;
        }
    }


     protected function buscar_hotdet(){

        $consulta = "select max(id_hotdet) as hotdet from  ".$this->info["bd"].".hotdet";
        $traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

        $this->info["hotdet"] = $traer->Fields("hotdet");

    }

	protected function buscar_bd(){

		$consulta = "select bd from hoteles.clientes where nombre ='".$this->session["cliente"]."'  ";
		$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

		$this->info["bd"] = trim($traer->Fields("bd"));

	} 

	protected function traer_hotel(){

        $consulta = " select id_hotel_".$this->session["cliente"]." as hotel from hoteles.hotelesmerge where id_pk = ".$this->info["pk"]." ";
        $traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

        $hotel = $traer->Fields("hotel");

        $this->info["hotel"] = $hotel;

    }


    protected function revisar_hab_global_porcliente(){

    	$retornar = 0;
        $consulta = " select id_tipohabitacion from ".$this->info["bd"].".tipohabitacion where id_tipohab = ".$this->info["habitacion"]." ";
        $traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

        if($traer->RecordCount() > 0){
        	$retornar = 1;
        	$this->info["habitacion"] = $traer->Fields("id_tipohabitacion");
        }

        return $retornar;
    }


	protected function errores($linea){
		die($_SERVER['REQUEST_URI']." - ".$linea." : ".$this->sql_con->ErrorMsg());
	}


	public function __destruct(){
		$this->sql_con->close();
		echo json_encode($this->datos);
	}

}