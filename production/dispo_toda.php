<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Nueva Empresa</title>

    <?php include("css.php");?>

    <link rel="stylesheet" href="css/protip.min.css">
    <link rel="stylesheet" type="text/css" href="css/select2.css">
    <style type="text/css">
       #datos{height : 400px !important;}
    </style>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        

        <?php include("menu_izquierdo.php");?>

        <!-- top navigation -->
        <?php include("menu_superior_derecho.php");?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3></h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Detalle de Disponibilidad</h2>
                    <ul class="nav navbar-right panel_toolbox  pull-rigth">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">



                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left fechas desde form_cot"  name="desde" id="desde" placeholder="Fecha Desde">
                        <span class="fa fa-calendar form-control-feedback left" aria-hidden="true"></span>
                      </div>

                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left fechas hasta form_cot" name="hasta" id="hasta" placeholder="Fecha Hasta">
                        <span class="fa fa-calendar form-control-feedback left" aria-hidden="true"></span>
                      </div>


                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <br><br>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group row">
                              <label for="ciudad" class="col-md-3 col-sm-3 col-xs-12 col-form-label">Ciudad</label>
                                <i class="fa fa-spinner fa-spin" id="spinner-ciudad"></i>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <select id="ciudad" name="ciudad" class="form-control select2 pull-left desactivados" >
                                  </select>
                                </div>
                              </div>
                           </div>

                           <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group row">
                              <label for="tipo_disponibilidad" class="col-md-3 col-sm-3 col-xs-12 col-form-label">Tipo de Disponibilidad</label>
                                <i class="fa fa-spinner fa-spin" id="spinner-tipo_disponibilidad"></i>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <select id="disponibilidad" name="disponibilidad" class="form-control select2 pull-left desactivados" >
                                      <option></option>
                                      <option value="0">Global</option>
                                      <option value="1">Normal</option>
                                  </select>
                                </div>
                              </div>
                           </div>
                            
                          
                      </div>


                       <div class="col-md-12 col-sm-12 col-xs-12">
                               <div class="col-md-12 col-sm-12 col-xs-12">
                                  <button id="buscando" class="btn btn-success btn-round desactivados pull-right">Buscar</button>
                              </div>
                              <br><hr><br>
                       </div>
                
                         
                        <div class="col-md-2 col-sm-2 col-xs-12 pull-right">
                          <input type="text" class='form-control kwd_search desactivados' placeholder = "BUSCAR"/>
                        </div>
                  

                        <div class="row"></div>
                        
                         <div class="col-md-12 col-sm-12 col-xs-12 centered">
                          <br>
                            <div class="col-md-10 col-sm-10 col-xs-12 col-md-offset-1">
                                <div  id="datos"></div>
                            </div>
                        
                         </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <input type="hidden" class="nombre_cliente">
        <!-- /page content -->

        <!-- footer content -->
        <?php include("footer.php");?>
        <!-- /footer content -->
      </div>
    </div>

    
    <?php include("jquery.php");?>

    <script type="text/javascript" src="js/protip.min.js"></script>
    <script type="text/javascript" src="js/select2.js"></script>
    <script type="text/javascript" src="js/tableHeadFixer.js"></script>
    
    <script type="text/javascript">
      


$(document).on("ready", function(){

    $.protip();

    $('#desde').daterangepicker({
      singleDatePicker: true,
      singleClasses: "desde",
      locale: {
        format: 'DD-MM-YYYY'
      },
      language: 'es'

    });

    $('#hasta').daterangepicker({
      singleDatePicker: true,
      singleClasses: "hasta",
      locale: {
        format: 'DD-MM-YYYY'
      },
      language: 'es'
    });


      $(".select2").attr("disabled","disabled");
      $(".select2").select2({
          language: "es",
          placeholder: "Seleccionar..",
          allowClear: true
      });



       $.post("sitio/Hotel.php",{tipo : 2},
           function (data) {

                  try{
                   data = $.parseJSON(data);
                 }catch(err){
                   mostrar_notificacion("ERROR", "Esta ocurriendo un error en el sistema.", "error","stack-bottomleft");
                   return false;
                 }
                  var ciudad = '<option></option>';
                  
                  $.each(data.ciudades, function (i, datos) {
                    if(datos.nombre_ciudad != "")
                      ciudad += '<option  value="' + datos.id_ciudad + '">' + datos.nombre_ciudad + '</option>';
                  });
                 
                  $("#ciudad").html(ciudad).removeAttr("disabled","disabled");
                  $("#spinner-ciudad, #spinner-tipo_disponibilidad").removeClass("fa-spinner fa-spin");
                  $("#disponibilidad").removeAttr("disabled","disabled")


           }
       );

    var pathname = window.location.href;
    var separar = pathname.replace("http://","");
    var ver = separar.split(".")[0];
  
    $(".desactivados").attr("disabled",true).addClass("disabled");
    $("#datos").html("<center><i class='fa fa-spinner fa-spin fa-2x'></i></center>");

    $.post("sitio/Hotel.php", {
                    tipo:8, operador : ""+ver+""
                },
                function (data) {

                    data = $.parseJSON(data);
                    tabla(data);

    }).done(function(){

      $(".desactivados").attr("disabled",false).removeClass("disabled");
      $('select#ciudad').val(96).trigger('change');


    });


     $('select').on('select2:open', function() {
      $('.select2-search--dropdown .select2-search__field').attr('placeholder', 'Buscar...');
    });


});


  $(document).on("keyup",".kwd_search",function(){
    
    if($(this).val() != ""){
      $("#tabla_detalle_dia tbody>tr").hide();
      $("#tabla_detalle_dia td:contains-ci('" + $(this).val() + "')").parent("tr").show();
    }else
      $("#tabla_detalle_dia tbody>tr").show();
    

  });



  $("#buscando").on("click", function(){

        var pathname = window.location.href;
        var separar = pathname.replace("http://","");
        var ver = separar.split(".")[0];

        var desde = $("#desde").val();
        var hasta = $("#hasta").val();
        var ciudad = $("#ciudad").val();
        var disponibilidad = $("#disponibilidad").val();

      $("#datos").html("<center><i class='fa fa-spinner fa-spin fa-2x'></i></center>");


        $.post("sitio/Hotel.php", {
                    tipo:8, desde : desde, hasta : hasta, ciudad : ciudad, operador : ""+ver+"", disponibilidad : disponibilidad
                },
                function (data) {

                    data = $.parseJSON(data);

                    if(data[desde] === undefined || data[desde] == ""){
                      mostrar_notificacion("Advertencia", "<label style='color:white !important;font-size:13px'>No hay hotel para la cuidad seleccionada.</label>", "warning", "bottom-left");
                      $("#datos").html("");
                    }else
                      tabla(data);
                    
              }

          );


     });



  function agregar_info(dias,data){
  
    $.each(data[dias], function(x,datos){
      var bgcolor = "";
            if(datos.cerrado == 0)
              bgcolor = "bg-default";
            else
              bgcolor = "bg-info";


            if(datos.modo == 0)
                $("#"+datos.id_pk+datos.dia+datos.habitacion).html('<small>Single : <b class="pull-right">'+datos.sgl+'</b><br> Twin : <b class="pull-right">'+datos.twn+'</b> <br> Matri. : <b class="pull-right">'+datos.dbl+'</b><br> Triple : <b class="pull-right">'+datos.tpl+'</b></small>').addClass(bgcolor).closest("tr").addClass("mostrar");
            else
                $("#"+datos.id_pk+datos.dia+datos.habitacion).html('<small><br><br>Habitación : <b class="pull-right">'+datos.sgl+'</b></small>').addClass(bgcolor).closest("tr").addClass("mostrar");
  

            bgcolor = "";

    });
    
  }

  function tabla(data){

        var th = "";
        var tr = "";

        $.each(data.dias, function(i,dias){
          th += "<td style='white-space: nowrap !important ;'><center><span class='badge progress-bar-warning'><i class='fa fa-calendar'></i>   <strong>"+dias.dias+"</strong></span></center></td>";
        });

  

        var tabla ='<table class="table table-bordered responsive" id="tabla_detalle_dia" width="100%"><thead>'
                        +'<tr>'
                          +'<td align="center">HOTEL</td>'
                          +th
                        +'</tr></thead><tbody>';

        var td = "";
      
        $.each(data[data.fecha_inicio], function(x,datos){

            var revisar_dispo_futura = "";
            var color = "";
            if(datos.dispo_futura == 0){
                revisar_dispo_futura = "danger";
                color = "";
            }else{
                revisar_dispo_futura = "success";
                color="data-pt-scheme='leaf'";
            }

            var d = '<div class="alert alert-success alert-dismissable"><small class="pull-left">'+datos.nombre_hotel+'</small><br> <b class="pull-right"><small>'+datos.th_nombre+'</small></b><br><br></div>'

            tabla += "<tr><td><i class='fa fa-info-circle pull-right text-"+revisar_dispo_futura+" protip' data-pt-position='bottom' data-pt-title='Disponibilidad futura : "+datos.dispo_futura+"' "+color+"></i>"+d+"</td>";
            $.each(data.dias, function(i,dias){
              td+= "<td id='"+datos.id_pk+dias.dias+datos.habitacion+"'></td>";
            });

            tabla += td+"</tr>";
            td = "";

        });

      
          tabla += "</tbody></table>"; 
          var cerrados = "<small class='pull-left badge progress-bar-info'>El color azul muestra los dís cerrados</small>";   
          var excel = "<small class='pull-right badge progress-bar-success'>Hoteles global <i class='fa fa-file-excel-o descargar_excel'></i></small><br>";            
          $("#datos").html(cerrados+excel+tabla);
          

          $.each(data.dias, function(i,dias){
            agregar_info(dias.dias,data);
          });

          var validar = 0;
          $("#tabla_detalle_dia tbody tr").each(function(){

            var tr = $(this).attr("class");
            
            if(tr === undefined)
              $(this).before().remove();

          });


      $("#tabla_detalle_dia").tableHeadFixer({"left" : 1}); 


  }


    $(document).on("click",".descargar_excel", function(){

        var pathname = window.location.href;
        var separar = pathname.replace("http://","");
        var ver = separar.split(".")[0];


        operador = ver;
        window.open("../../h2o/excel.php?verificando_sitio=1&operador="+operador+"&tipo=2",'_blank'); 

    });

    $.extend($.expr[":"], 
    {
        "contains-ci": function(elem, i, match, array) 
      {
        return (elem.textContent || elem.innerText || $(elem).text() || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
      }
    });


    </script>

  </body>
</html>
