<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Nueva Empresa</title>

    <?php include("css.php");?>

    <link rel="stylesheet" type="text/css" href="css/select2.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        

        <?php include("menu_izquierdo.php");?>

        <!-- top navigation -->
        <?php include("menu_superior_derecho.php");?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3></h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Buscar Hoteles</h2>
                    <ul class="nav navbar-right panel_toolbox  pull-rigth">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      
                  <br>
                    <form class="form-horizontal form-label-left input_mask">

                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left form_cot" name="nombre_hotel" id="nombre_hotel" placeholder="Nombre Hotel">
                        <span class="fa fa-file-o form-control-feedback left" aria-hidden="true"></span>
                      </div>

                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left form_cot" name="id_hotel" id="id_hotel" placeholder="Id Hotel">
                        <span class="fa fa-file-o form-control-feedback left" aria-hidden="true"></span>
                      </div>

                      <div class="col-md-12 col-sm-12 col-xs-12">
                      <br><br>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="form-group row">
                            <label for="pais" class="col-md-3 col-sm-3 col-xs-12 col-form-label">Pais</label>
                              <i class="fa fa-spinner fa-spin" id="spinner-pais"></i>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <select name="pais" class="form-control select2 form_cot" id="pais">
                                </select>
                              </div>
                            </div>
                         </div>

                         <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="form-group row">
                            <label for="ciudad" class="col-md-3 col-sm-3 col-xs-12 col-form-label">Ciudad</label>
                              <i class="fa fa-spinner fa-spin" id="spinner-ciudad"></i>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <select name="ciudad" class="form-control select2 form_cot" id="ciudad">
                                </select>
                              </div>
                          </div>
                        </div>
                      </div>


                      <div class="col-md-12 col-sm-12 col-xs-12">

                         <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="form-group row">
                            <label for="hotel" class="col-md-3 col-sm-3 col-xs-12 col-form-label">Hotel</label>
                              <i class="fa fa-spinner fa-spin" id="spinner-hotel"></i>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <select name="hotel" class="form-control select2 form_cot" id="hoteles">
                                </select>
                              </div>
                          </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="form-group row">
                            <label for="estado" class="col-md-3 col-sm-3 col-xs-12 col-form-label">Estado</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <select name="estado" class="form-control select2 form_cot" id="estado">
                                     <option></option>
                                     <option value="0">Activos</option>
                                     <option value="1">Desactivados</option>
                                </select>
                              </div>
                          </div>
                        </div>


                      </div>

                      
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <hr>
                          <button class="btn btn-success btn-round pull-right" id="buscar">Buscar</button>
                        </div>
                      </div>

                    </form>
                    <br><br>
                    <hr>
                  </div>



                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div id="buscando_hoteles"></div>
                  </div>


                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <hr>
                    <div id="buscando_tarifas"></div>
                  </div>

                      
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div id="contenido_tabla_tarifa" style="display:none;"></div>
        <input type="hidden" id="pk">

        <!-- /page content -->

        <!-- footer content -->
        <?php include("footer.php");?>
        <!-- /footer content -->
      </div>
    </div>

    <input type="hidden" class="nombre_cliente">
    
    <?php include("jquery.php");?>

    <script type="text/javascript" src="js/select2.js"></script>
    <script type="text/javascript" src="js/bootbox.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>


    <script>

    $(document).on("ready",function(){

            $(".select2").attr("disabled","disabled");
            $(".select2").select2({
                language: "es",
                placeholder: "Seleccionar..",
                allowClear: true
            });



             $.post("sitio/Hotel.php",{tipo : 2},
                 function (data) {

                        try{
                         data = $.parseJSON(data);
                       }catch(err){
                         mostrar_notificacion("ERROR", "Esta ocurriendo un error en el sistema.", "error","stack-bottomleft");
                         return false;
                       }
                        var ciudad = '<option></option>';
                        
                        $.each(data.ciudades, function (i, datos) {
                          if(datos.nombre_ciudad != "")
                            ciudad += '<option  value="' + datos.id_ciudad + '">' + datos.nombre_ciudad + '</option>';
                        });
                       
                        $("#ciudad").html(ciudad).removeAttr("disabled","disabled");
                        $("#spinner-ciudad").removeClass("fa-spinner fa-spin");


                 }
             );



             $.post("sitio/Hotel.php",{tipo : 6},
                 function (data) {

                        try{
                         data = $.parseJSON(data);
                       }catch(err){
                         mostrar_notificacion("ERROR", "Esta ocurriendo un error en el sistema.", "error","stack-bottomleft");
                         return false;
                       }
                        var pais = '<option></option>';
                        
                        $.each(data.paises, function (i, datos) {
                          if(datos.nombre_pais != "")
                            pais += '<option  value="' + datos.id_pais + '">' + datos.nombre_pais + '</option>';
                        });
                       
                        $("#pais").html(pais).removeAttr("disabled","disabled");
                        $("#spinner-pais").removeClass("fa-spinner fa-spin");


                 }
             );


             $.post("sitio/Hotel.php",{tipo : 1},
                 function (data) {

                        try{
                         data = $.parseJSON(data);
                       }catch(err){
                         mostrar_notificacion("ERROR", "Esta ocurriendo un error en el sistema.", "error","stack-bottomleft");
                         return false;
                       }
                        var hotel = '<option></option>';
                        
                        $.each(data.hoteles, function (i, datos) {
                          hotel += '<option  value="' + datos.pk + '">' + datos.nombre_hotel + '</option>';
                        });
                       
                        $("#hoteles").html(hotel).removeAttr("disabled","disabled");
                        $("#estado").removeAttr("disabled","disabled");
                        $("#spinner-hotel").removeClass("fa-spinner fa-spin");


                 }
        );


        $('select').on('select2:open', function() {
          $('.select2-search--dropdown .select2-search__field').attr('placeholder', 'Buscar...');
        });

        
     });


    $("#buscar").on("click", function(e){
        
        e.preventDefault();
        $(this).html("<i class='fa fa-spinner fa-spin'></i>").addClass("disabled");
        $("#buscando_hoteles,#buscando_tarifas").html("");


        var nombre_hotel = $("#nombre_hotel").val();
        var id_hotel = $("#id_hotel").val();
        var pais = $("#pais").val();
        var ciudad = $("#ciudad").val();
        var hoteles = $("#hoteles").val();
        var estado = $("#estado").val();



        $.post("sitio/Hotel.php",{tipo : 7,nombre_hotel:nombre_hotel,id_hotel:id_hotel,pais:pais,ciudad:ciudad,hoteles:hoteles,estado:estado},
                 function (data) {

                        try{
                         data = $.parseJSON(data);
                       }catch(err){
                         mostrar_notificacion("ERROR", "Esta ocurriendo un error en el sistema.", "error","stack-bottomleft");
                         return false;
                       }


                       var tabla ='<table class="table table-bordered table-striped" id="tabla_hoteles" width="100%"><thead>'
                              +'<tr>'
                                  +'<td align="center"><strong>Id Hotel</strong></td>'
                                  +'<td align="center" style="white-space: nowrap !important;"><strong>Nombre Hotel</strong></td>'
                                  +'<td align="center" style="white-space: nowrap !important;"><strong>Dirección</strong></td>'
                                  +'<td align="center"><strong>País</strong></td>'
                                  +'<td align="center"><strong>Ciudad</strong></td>'
                                  +'<td align="center"><strong>Comuna</strong></td>'
                                  +'<td align="center"><strong>Fono</strong></td>'
                                  +'<td align="center"><strong>Email</strong></td>'
                                  +'<td align="center"><strong>Estado</strong></td>'
                                  +'<td align="center" style="white-space: nowrap !important;"><strong>Acción</strong></td>'
                              +'</tr></thead><tbody>';

                        
                        $.each(data.hoteles_operador, function (i, datos) {

                          var estado = "<td align='center'><small>Desactivado</small></td>";

                          if(datos.estado == 0)
                            estado = "<td align='center'><small>Activado</small></td>";


                           tabla += "<tr>"
                                      +"<td align='center'><small>"+datos.id_hotel+"</small></td>"
                                      +"<td align='center'><small>"+datos.nombre_hotel+"</small></td>"
                                      +"<td align='center'><small>"+datos.direccion+"</small></td>"
                                      +"<td align='center'><small>"+datos.pais+"</small></td>"
                                      +"<td align='center'><small>"+datos.comuna+"</small></td>"
                                      +"<td align='center'><small>"+datos.ciudad+"</small></td>"
                                      +"<td align='center'><small>"+datos.fono+"</small></td>"
                                      +"<td align='center'><small>"+datos.email+"</small></td>"
                                      +estado
                                      +"<td align='center'><a title='Editar'><i class='fa fa-edit pull-left text-warning editar' data-hotel='"+datos.id_hotel+"' data-pk='"+datos.id_pk+"' data-nombre_hotel='"+datos.nombre_hotel+"'></i></a><a title='Eliminar'><i class='fa fa-trash text-danger pull-right'></i></a></td>"
                                  +"</tr>"   

                        });

                         if(data.hoteles_operador === undefined || data.hoteles_operador == "" || data.hoteles_operador === null)
                            tabla = '<div class="alert alert-danger alert-dismissible fade in" role="alert">No existen reservas para el filtro ingresado.</div>';


                        $("#buscando_hoteles").html(tabla);


          }).done(function(){
            $("#buscar").html("Buscar").removeClass("disabled");
            $("#tabla_hoteles").DataTable({
              "scrollY":        "400px",
              "scrollX" : "100%",
              "scrollCollapse": true,
                    "aaSorting": [[0, 'asc']],
                    "oLanguage": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ ",
                        "sZeroRecords": "No se han encontrado datos disponibles",
                        "sEmptyTable": "No se han encontrado datos disponibles",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando  del 0 al 0 de un total de 0 ",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ datos)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar: ",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Última",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    }

             });


        });



    });


    $(document).on("click",".editar", function(){

        var id_hotel = $(this).data("hotel");
        var nombre_hotel = $(this).data("nombre_hotel");
        $("#pk").val($(this).data("pk"));
        $(this).removeClass("fa-edit").addClass("fa-spinner fa-spin");


        $.post("sitio/Data.php",{tipo : 2,id_hotel:id_hotel,id_hotel},
                 function (data) {

                        try{
                         data = $.parseJSON(data);
                       }catch(err){
                         mostrar_notificacion("ERROR", "Esta ocurriendo un error en el sistema.", "error","stack-bottomleft");
                         return false;
                       }


                       var tabla ='<table class="table table-bordered table-striped" id="tabla_tarifas" width="100%"><thead>'
                              +'<tr>'
                                  +'<td align="center" style="white-space: nowrap !important;"><strong>Tipo Tarifa</strong></td>'
                                  +'<td align="center" style="white-space: nowrap !important;"><strong>Fecha Desde</strong></td>'
                                  +'<td align="center" style="white-space: nowrap !important;"><strong>Fecha Hasta</strong></td>'
                                  +'<td align="center"><strong>Single</strong></td>'
                                  +'<td align="center"><strong>Doble Twin</strong></td>'
                                  +'<td align="center"><strong>Doble Matrimonial</strong></td>'
                                  +'<td align="center"><strong>Triple</strong></td>'
                                  +'<td align="center" style="white-space: nowrap !important;"><strong>Acción</strong></td>'
                              +'</tr></thead><tbody>';

                        
                        $.each(data.tarifas, function (i, datos) {

                           tabla += "<tr>"
                                      +"<td align='left'><small>"+datos.tipo_tarifa+" ("+datos.id_hotdet+")</small></td>"
                                      +"<td align='center'><small>"+datos.desde+"</small></td>"
                                      +"<td align='center'><small>"+datos.hasta+"</small></td>"
                                      +"<td align='center'><small>"+datos.sgl+"</small></td>"
                                      +"<td align='center'><small>"+datos.twn+"</small></td>"
                                      +"<td align='center'><small>"+datos.dbl+"</small></td>"
                                      +"<td align='center'><small>"+datos.tpl+"</small></td>"
                                      +"<td align='center'><a title='Editar'><i class='fa fa-edit pull-left text-warning editar_tarifas' data-hotdet='"+datos.id_hotdet+"'></i></a><a title='Eliminar'><i class='fa fa-trash text-danger pull-right'></i></a></td>"
                                  +"</tr>"   

                        });

                         if(data.tarifas === undefined || data.tarifas == "" || data.tarifas === null){
                            $(".editar").removeClass("fa-spinner fa-spin").addClass("fa-edit");
                            tabla = '<div class="alert alert-warning alert-dismissible fade in" role="alert">No existen tarifas ingresadas para el hotel.</div>';
                         }

                        
                        var agregar_info = '<div class="panel panel-warning">'
                                           +'<div class="panel-heading"><center><strong>TARIFAS VIGENTES '+nombre_hotel.toUpperCase()+'</strong></center></div>'
                                              +'<div class="panel-body">'
                                                  +'<button class="btn btn-default pull-left" id="nueva_tarifa">Nueva tarifa</button><br><br><br>'
                                                  +'<div id="informacion_tarifas">'+tabla+'</div>'
                                              +'</div>'
                                            +'</div>'
                                          +'</div>';
                       
                       
                        $("#buscando_tarifas").html(agregar_info);


          }).done(function(){
            $("#buscar").html("Buscar").removeClass("disabled");
            $(".editar").removeClass("fa-spinner fa-spin").addClass("fa-edit");
            $("#tabla_tarifas").DataTable({
              "scrollY":"400px",
              "scrollX" : "100%",
              "scrollCollapse": true,
                    "aaSorting": [[0, 'asc']],
                    "oLanguage": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ ",
                        "sZeroRecords": "No se han encontrado datos disponibles",
                        "sEmptyTable": "No se han encontrado datos disponibles",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando  del 0 al 0 de un total de 0 ",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ datos)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar: ",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Última",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    }

             });


        });


    });


$(document).on("click","#nueva_tarifa",function(){

	$(this).html("<i class='fa fa-arrow-left'></i>").attr("id","volver").removeClass("btn-default").addClass("btn-warning");
	var formulario = formularios(1);
	$("#contenido_tabla_tarifa").html($("#informacion_tarifas").html());

    $("#informacion_tarifas").html("");
    $("#informacion_tarifas").html(formulario);

    var pk = $("#pk").val();

     $.post("sitio/Data.php",{tipo : 3},
         function (data) {

                try{
                 data = $.parseJSON(data);
               }catch(err){
                 mostrar_notificacion("ERROR", "Esta ocurriendo un error en el sistema.", "error","stack-bottomleft");
                 return false;
               }
                var tarifa = '<option></option>';
                
                $.each(data.tipo_tarifas, function (i, datos) {
                  if(datos.tipo_tarifa != "")
                    tarifa += '<option  value="' + datos.id_tipotarifa + '">' + datos.tipo_tarifa + '</option>';
                });
               
                $("#tarifa").html(tarifa).removeAttr("disabled","disabled");


         }
     );


     $.post("sitio/Data.php",{tipo : 4, pk:pk},
         function (data) {

                try{
                 data = $.parseJSON(data);
               }catch(err){
                 mostrar_notificacion("ERROR", "Esta ocurriendo un error en el sistema.", "error","stack-bottomleft");
                 return false;
               }
                var habitacion = '<option></option>';
                
                $.each(data.tipo_habitacion, function (i, datos) {
                  if(datos.nombre_tipohab != "")
                    habitacion += '<option  value="' + datos.id_tipohabitacion + '">' + datos.nombre_tipohab + '</option>';
                });
               
                $("#habitacion").html(habitacion).removeAttr("disabled","disabled");


         }
     );




    $('#desde').daterangepicker({
      singleDatePicker: true,
      singleClasses: "desde",
      locale: {
        format: 'DD-MM-YYYY'
      },
      language: 'es'

    });

    $('#hasta').daterangepicker({
      singleDatePicker: true,
      singleClasses: "hasta",
      locale: {
        format: 'DD-MM-YYYY'
      },
      language: 'es'
    });

     
    $(".select2").select2({
        language: "es",
        placeholder: "Seleccionar..",
        allowClear: true
    });

	$('select').on('select2:open', function() {
      $('.select2-search--dropdown .select2-search__field').attr('placeholder', 'Buscar...');
    });

});


$(document).on("click","#volver",function(){
	$(this).html("Nueva Tarifa").attr("id","nueva_tarifa").removeClass("btn-warning").addClass("btn-default");
	$("#informacion_tarifas").html($("#contenido_tabla_tarifa").html());
});


$(document).on("change","#dia_desde", function(){

    var dia_desde = $(this).val();
    var dia_hasta = $("#dia_hasta").val();

    var i;
    $(".activar_dias").prop("checked",false);


    if(dia_hasta>dia_desde){

        for(i=dia_hasta;i<=dia_hasta;i++){

           $("#dia_"+i).prop("checked",true); 

           console.log(i);
        }

        


    }else{

        for(i=dia_desde;i<=dia_hasta;i++){

           $("#dia_"+i).prop("checked",true); 

           console.log(i);
        }

    }

    $(".activar_dias:not(:checked)").each(function(){
        $(this).prop("disabled",true);
    });

     $(".activar_dias:checked").each(function(){
        $(this).prop("disabled",false);
    });

});


$(document).on("change","#dia_hasta", function(){

        var dia_hasta = $(this).val();
        var dia_desde = $("#dia_desde").val();

        var i;
        $(".activar_dias").prop("checked",false);


        if(dia_desde>dia_hasta){

              $("#dia_"+dia_desde).prop("checked",true);

             for(i=1;i<=dia_hasta;i++){

               $("#dia_"+i).prop("checked",true); 

               console.log(i);
            }   


        }else{
        
            for(i=dia_desde;i<=dia_hasta;i++){

               $("#dia_"+i).prop("checked",true);
               console.log(i); 
            }

        }

        $(".activar_dias:not(:checked)").each(function(){
            $(this).prop("disabled",true);
        });

        $(".activar_dias:checked").each(function(){
            $(this).prop("disabled",false);
        });

 });



    $(document).on("keyup","input", function(){
         if(!$(this).val())
              $(this).css("border-color","red");
         else
            $(this).css("border-color","silver");
        
     });


    $(document).on("change","select", function(){
         if(!$(this).val())
              $(this).css("border-color","red");
         else
            $(this).css("border-color","silver");
        
     });


$(document).on("keyup",".precios", function(){

        var id = $(this).attr("id");
        var verificar = $(this).attr("data-id");

        if(verificar == 1){


                if($("#"+id).val() > 1000){
                     
                     $("#"+id).val("");  
                     mostrar_notificacion("Aviso", "<label style='color:white !important;font-size:13px'>El precio USD no puede superar los 1.000.</label>", "warning", "bottom-left");

                }

        }

        var valor = $(this).val();

        if(valor==""){
            $("#"+id).css("border-color","red").val("");
        }else{
            var contar = 0;
            $.each($(".precios"), function(){
                contar+=1;
                var id_comparar = $(this).attr("id");
                var valor_comparar = $(this).val();
                
                if(valor_comparar=="")
                    $("#"+id_comparar).val(0);
            });

            $(".precios").css("border-color","silver");
        }
 
});

$(document).on("click","#enviar_form", function(){


        $(this).html("Creando tarifas... <i class='fa fa-spinner fa-spin'></i>").addClass("disabled");
        var editar = $(this).data("editar");
        var hotdet = $(this).data("hotdet");


         var tarifas = $(".tarifas");

                 $.each(tarifas, function(){

                        var validos = $(this).attr("data-id");

                        if(validos == 1){


                            var id = $(this).attr("id");

                            var valor = $("#"+id).val();

                            if(valor == 0 || valor == ""){

                                $("#"+id).css("border-color","red");
                                $("#error_"+id).css("display","block");


                            }else{

                                $("#"+id).attr("border-color","");
                                $("#error_"+id).css("display","none");

                            }

                        }


                        


                 });



            var area = $(".area");

                 $.each(area, function(){

                        var validos = $(this).attr("data-id")

                        if(validos == 1){


                            var id = $(this).attr("id");

                            var valor = $("#"+id).val();

                            if(valor == 0 || valor == ""){


                                $("#"+id).css("border-color","red");


                            }else{

                                $("#"+id).css("border-color","silver");
                                
                            }

                        }


                        


                 });


                 var habitaciones = $(".habitaciones");

                 $.each(habitaciones, function(){

                        var validos = $(this).attr("data-id");



                        if(validos == 1){


                            var id = $(this).attr("id");

                            var valor = $("#"+id).val();

                            if(valor == 0 || valor == ""){


                                 $("#error_"+id).css("display","block");


                            }else{

                                $("#error_"+id).css("display","none");
                                
                            }

                        }


                        


                 });



                 var tarifas2 = $(".tarifas2");

                 $.each(tarifas2, function(){

                        var validos = $(this).attr("data-id");



                        if(validos == 1){


                            var id = $(this).attr("id");

                            var valor = $("#"+id).val();

                            if(valor == 0 || valor == ""){


                                 $("#"+id).css("border-color","red");


                            }else{

                                $("#"+id).css("border-color","silver");
                            }

                        }


                        


                 });


                 var regimen = $(".regimen");

                 $.each(regimen, function(){

                        var validos = $(this).attr("data-id");



                        if(validos == 1){


                            var id = $(this).attr("id");

                            var valor = $("#"+id).val();

                            if(valor == 0 || valor == ""){


                                 $("#"+id).css("border-color","red");


                            }else{

                                $("#"+id).css("border-color","silver");
                            }

                        }


                        


                 });





                 $(".tarifas").on("change", function(){

                    var valor = $(this).val();
                    var id = $(this).attr("id");

                        if(valor!="" || valor > 0){

                            $("#error_"+id).css("display","none");

                        }else{

                            $("#error_"+id).css("display","block");

                        }

                 });


                 $(".habitaciones").on("change", function(){

                    var valor = $(this).val();
                    var id = $(this).attr("id");

                        if(valor!="" || valor > 0){

                            $("#error_"+id).css("display","none");

                        }else{

                            $("#error_"+id).css("display","block");

                        }

                 });


                 $(".area").on("change", function(){

                    var valor = $(this).val();
                    var id = $(this).attr("id");

                        if(valor!="" || valor > 0){

                            $("#"+id).css("border-color","silver");

                        }else{

                            $("#"+id).css("border-color","red");

                        }

                 });


                 $(".tarifas2").on("change", function(){

                    var valor = $(this).val();
                    var id = $(this).attr("id");

                        if(valor!="" || valor > 0){

                            $("#"+id).css("border-color","silver");

                        }else{

                            $("#"+id).css("border-color","red");

                        }

                 });

                 $(".regimen").on("change", function(){

                    var valor = $(this).val();
                    var id = $(this).attr("id");

                        if(valor!="" || valor > 0){

                            $("#"+id).css("border-color","silver");

                        }else{

                            $("#"+id).css("border-color","red");

                        }

                 });



                 var formulario = $(".formulario");
                 var errores = 0;
                 var guardar = [];
                 
                 $.each(formulario, function(){

                     var operador = $(this).attr("id");
                     var verificar = $(this).attr("data-id");
                     var cliente = $(this).attr("data-cliente");

                     if(verificar == 1){
                          var obj = {};
                          $("#"+operador+" select").map(function(){
                             if( !$(this).val() ) {
                                  errores++;
                            }else{
                                var nombre = $(this).attr("id");
                                obj["cliente"] = cliente;
                                obj[""+nombre+""] = $(this).val();
                               
                            } 
                        });

                        $("#"+operador+" textarea").map(function(){
                             if( !$(this).val() ) {
                                   //$("#desc_tarifa").css("border-color","red");
                            }else{
                                var nombre = $(this).attr("id");
                                obj[""+nombre+""] = $(this).val();
                                //$("#desc_tarifa").css("border-color","lver");
                            }  
                        });

                        guardar.push(obj);

                    }
                     
                     

                 });


                 var formulario_todo = $(".form_todo");
                 var errores_todo = 0;
                 var guardar_todo = [];
                 
                 $.each(formulario_todo, function(){
                    var obj2 = {};
                     $(".form_todo input").map(function(){
                         if( !$(this).val() ) {
                                  errores_todo++;
                                  $(this).css("border-color","red");
                            }else{
                                var nombre = $(this).attr("id");
                                obj2[""+nombre+""] = $(this).val();
                                $(this).css("border-color","silver");
                               
                            }  
                     });

                      $(".form_todo select").map(function(){
                         if( !$(this).val() ) {
                                  errores_todo++;
                                  $(this).css("border-color","red");
                            }else{
                                var nombre = $(this).attr("id");
                                obj2[""+nombre+""] = $(this).val();
                                $(this).css("border-color","silver");
                            }  
                     });

                     guardar_todo.push(obj2);

                 });


                 var activar_dias = $(".activar_dias");
                 var guardar_todo_dias = [];
                 var d = 0;
                 $.each(activar_dias, function(){
                       
                    var dias ={};
                    if($(this).is(':checked')){

                        d+=1; 
                        var id = $(this).attr("data-id");
                        dias["dias"] = id;
                          
                    }
                    guardar_todo_dias.push(dias);
                    

                 });


                 var valor_sgl = $("#p_sgl").val();
                 var valor_dbl = $("#p_dbl").val();
                 var valor_tpl = $("#p_tpl").val();

		        if(parseInt(valor_dbl) > parseInt(valor_sgl) && parseInt(valor_dbl) == parseInt(valor_sgl)){
		              mostrar_notificacion("Aviso", "<label style='color:white !important;font-size:13px'>Valor DBL debe ser por persona, favor verificar.</label>", "warning", "bottom-left");
		              $("#enviar_form").html("Cargar tarifas").removeClass("disabled");
		        }else if(parseInt(valor_tpl) > parseInt(valor_sgl) && parseInt(valor_tpl) == parseInt(valor_sgl)){
		              mostrar_notificacion("Aviso", "<label style='color:white !important;font-size:13px'>Valor TPL debe ser por persona, favor verificar.</label>", "warning", "bottom-left");
		              $("#enviar_form").html("Cargar tarifas").removeClass("disabled");
		        }else{


                    if(errores == 0 && errores_todo == 0){

                            bootbox.confirm({
                            title: "¿Ha completado correctamente los campos?",
                            message: "Recordar que en Distantis las tarifas deben ser cargadas por persona (NO por habitación), sin comisión y sin IVA.",
                            buttons: {
                                cancel: {
                                    label: '<i class="fa fa-times"></i> Cancelar',
                                    className: 'btn-danger'
                                },
                                confirm: {
                                    label: '<i class="fa fa-check"></i> Confirmar',
                                    className: 'btn-success'
                                }
                            },
                                callback: function (result) {

                                    if(result){

                                          var pk = $("#pk").val();

                                           $.post("sitio/DataAll.php", {
                                                        tipo: 1, informacion : guardar_todo, informacion_operador : guardar, dias_cerrar : guardar_todo_dias,pk :pk, editar:editar,hotdet:hotdet
                                                    },

                                                 function (data) {

                                                        try{
                    									                       data = $.parseJSON(data);
                    									                     }catch(err){
                    									                       mostrar_notificacion("ERROR", "<label style='color:white !important;font-size:13px'>Esta ocurriendo un error en el sistema.</label>", "danger", "bottom-left");
                    									                     }


									                                 switch(data.respuesta){

                									                     	case 1:
                		                     		                      mostrar_notificacion("Tarifas", "Se ha cargado correctamente la tarifa.", "success", "bottom-left");
                                                                  $("#enviar_form").html("Cargar tarifas").removeClass("disabled");
                                                                  $("#volver").html("Nueva Tarifa").attr("id","nueva_tarifa").removeClass("btn-warning").addClass("btn-default");
                									                                $("#informacion_tarifas").html($("#contenido_tabla_tarifa").html());
                									                     	break;

                									                     	case 2:
                                                                	mostrar_notificacion("Aviso", "<label style='color:white !important;font-size:13px'>Debe seleccionar una fecha vigente.</label>", "warning", "bottom-left");
                                                                  $("#enviar_form").html("Cargar tarifas").removeClass("disabled");
                									                     	break;

                									                     	case 3:
                                                                	mostrar_notificacion("Aviso", "La categoría de habitación no esta correctamente mapeada, favor contactarnos para arreglar el problema.", "warning", "bottom-left");
                                                                  $("#enviar_form").html("Cargar tarifas").removeClass("disabled");
                									                     	break;

                                                        case 4:
                                                                  mostrar_notificacion("Tarifas", "Se ha actualizado correctamente la tarifa.", "success", "bottom-left");
                                                                  $("#enviar_form").html("Cargar tarifas").removeClass("disabled");
                                                                  $("#volver").html("Nueva Tarifa").attr("id","nueva_tarifa").removeClass("btn-warning").addClass("btn-default");
                                                                  $("#informacion_tarifas").html($("#contenido_tabla_tarifa").html());
                                                        break;

                									                     	default:
                		                     		                      mostrar_notificacion("Tarifas", "Ha ocurrido un problema al intentar cargar la tarifa.", "warning", "bottom-left");
                                                                  $("#enviar_form").html("Cargar tarifas").removeClass("disabled"); 
                									                     	break;

									                               }

                                          }    
                                     	);
                                 
                                    }else
                                        $("#enviar_form").html("Cargar tarifas").removeClass("disabled");
                                    
                                }
                            });

                        }else if(d==0){
                          $("#dia_desde").css("border-color","red").val("");
                          $("#dia_hasta").css("border-color","red").val("");  
                          mostrar_notificacion("Aviso", "<label style='color:white !important;font-size:13px'>Debe seleccionar días de la semana.</label>", "warning", "bottom-left");
                          $("#enviar_form").html("Cargar tarifas").removeClass("disabled");
                        }else{

                          mostrar_notificacion("Aviso", "<label style='color:white !important;font-size:13px'>Faltan campos.</label>", "warning", "bottom-left");
                          $("#enviar_form").html("Cargar tarifas").removeClass("disabled");
                        }

        		}
          

               
     });



$(document).on("click",".editar_tarifas", function(){

	 var tarifa = $(this).data("hotdet");
   $(this).removeClass("fa-edit").addClass("fa-spinner fa-spin");

	 $.post("sitio/Data.php",{tipo : 5, tarifa:tarifa},
         function (data) {

                try{
                 data = $.parseJSON(data);
               }catch(err){
                 mostrar_notificacion("ERROR", "Esta ocurriendo un error en el sistema.", "error","stack-bottomleft");
                 return false;
               }
           	
              $("#nueva_tarifa").html("<i class='fa fa-arrow-left'></i>").attr("id","volver").removeClass("btn-default").addClass("btn-warning");
      			  var formulario = formularios(1);
      			  $("#contenido_tabla_tarifa").html($("#informacion_tarifas").html());

      		    $("#informacion_tarifas").html("");
      		    $("#informacion_tarifas").html(formulario);
              $("#enviar_form").attr("data-editar","1").html("Actualizar Tarifa").attr("data-hotdet",""+tarifa+"");
              $("#mensaje_stock").addClass("badge bg-red").html("* Recordar al editar fechas se eliminará el stock creado anteriormente.");

      		    var pk = $("#pk").val();


      		    $.post("sitio/Data.php",{tipo : 3},
      			         function (data_tarifa) {

      			                try{
      			                 data_tarifa = $.parseJSON(data_tarifa);
      			               }catch(err){
      			                 mostrar_notificacion("ERROR", "Esta ocurriendo un error en el sistema.", "error","stack-bottomleft");
      			                 return false;
      			               }
      			                var tarifa = '<option></option>';
      			                
      			                $.each(data_tarifa.tipo_tarifas, function (i, datos) {
      			                  if(datos.tipo_tarifa != "")
      			                    tarifa += '<option  value="' + datos.id_tipotarifa + '">' + datos.tipo_tarifa + '</option>';
      			                });
      			               
      			                $("#tarifa").html(tarifa).removeAttr("disabled","disabled").val(data.tarifa_cliente["id_tipotarifa"]).trigger('change');


      			         }
      			     );


      			     $.post("sitio/Data.php",{tipo : 4, pk:pk},
      			         function (data_tipohabitacion) {

      			                try{
      			                 data_tipohabitacion = $.parseJSON(data_tipohabitacion);
      			               }catch(err){
      			                 mostrar_notificacion("ERROR", "Esta ocurriendo un error en el sistema.", "error","stack-bottomleft");
      			                 return false;
      			               }
      			                var habitacion = '<option></option>';
      			                
      			                $.each(data_tipohabitacion.tipo_habitacion, function (i, datos) {
      			                  if(datos.nombre_tipohab != "")
      			                    habitacion += '<option  value="' + datos.id_tipohabitacion + '">' + datos.nombre_tipohab + '</option>';
      			                });
      			               
      			                $("#habitacion").html(habitacion).removeAttr("disabled","disabled").val(data.tarifa_cliente["id_tipohab"]).trigger('change');


      			         }
      			     );


      			   $('#desde').daterangepicker({
      			      singleDatePicker: true,
      			      singleClasses: "desde",
      			      locale: {
      			        format: 'DD-MM-YYYY'
      			      },
      			      language: 'es'

      			    });

      		    $('#hasta').daterangepicker({
      		      singleDatePicker: true,
      		      singleClasses: "hasta",
      		      locale: {
      		        format: 'DD-MM-YYYY'
      		      },
      		      language: 'es'
      		    });

      		     
      		    $(".select2").select2({
      		        language: "es",
      		        placeholder: "Seleccionar..",
      		        allowClear: true
      		    });


		    // Datos para editar

            $("#moneda").val(data.tarifa_cliente["hd_mon"]);
            $("#p_sgl").val(data.tarifa_cliente["hd_sgl"]);
            $("#p_dbl").val(data.tarifa_cliente["hd_dbl"]);
            $("#p_tpl").val(data.tarifa_cliente["hd_tpl"]);
            $("#desde").val(data.tarifa_cliente["hd_fecdesde"]);
            $("#hasta").val(data.tarifa_cliente["hd_fechasta"]);
            $("#dia_desde").val(data.tarifa_cliente["hd_diadesde"]);
            $("#dia_hasta").val(data.tarifa_cliente["hd_diahasta"]);
            $("#area").val(data.tarifa_cliente["id_area"]);


      //
      

    			   $('select').on('select2:open', function() {
    		      $('.select2-search--dropdown .select2-search__field').attr('placeholder', 'Buscar...');
    		    });


         }


     ).done(function(){
        $(".editar_tarifas").removeClass("fa-spinner fa-spin").addClass("fa-edit");
     });


});

function formularios(tipo){

  var formulario = "";

  switch(tipo){

    case 1:
      formulario ='<hr><div class="form_todo">'
                            +'<div class="col-md-12">'
                              +'<div class="col-md-4">'

                                    +'<div class="form-group">'

                                        +'<select name = "" id="moneda" class="form-control">'

                                          +'<option value="">Seleccionar Moneda</option>'
                                          +'<option value="1">USD</option>'
                                          +'<option value="2">CLP</option>'

                                        +'</select>'

                                      +'</div>'

                              +'</div>'

                              +'<div class="col-md-2">'

                                +'<div class="form-group">'
                                  +'<input type="tex" class="form-control precios " id="p_sgl" placeholder="Precio SGL">'
                                +'</div>'

                              +'</div>'

                              +'<div class="col-md-2">'

                                +'<div class="form-group">'
                                  +'<input type="tex" class="form-control precios" id="p_dbl" placeholder="Precio DBL">'
                                +'</div>'

                              +'</div>'

                              +'<div class="col-md-2">'

                                +'<div class="form-group">'
                                  +'<input type="tex" class="form-control precios" id="p_tpl" placeholder="Precio TPL">' 
                                +'</div>'
                              +'</div>'

                            +'</div>'



                            +'<div class="col-md-12">'
                              +'<div class="col-md-2">'
                                +'<div class="form-group">'
                                  +'<input type="text" class="form-control desde" name="fecha_inicio" id="desde" placeholder="Fecha desde" data-date-format="dd-mm-yyyy">'
                                +'</div>'
                                +'</div>'
                                
                                +'<div class="col-md-2">'
                                +'<div class="form-group">'
                                  +'<input type="text" class="form-control hasta" name="fecha_hasta"  id="hasta" placeholder="Fecha hasta" data-date-format="dd-mm-yyyy">'
                                +'</div>'
                                +'</div>'

                                +'<div class="col-md-2">'
                                +'<div class="form-group">'
                                  +'<input type="text" class="form-control" name="min_noche"  id="min_noche" placeholder="Mínimo Noches">'
                                +'</div>'
                                +'</div>'
                              +'</div>'

                              +'<div class="col-md-12">'
                              +'<div class="col-md-2">'
                                 +'<div class="form-group">'
                                  +'<strong>Día desde :</strong>'
                                +'</div>'
                                +'<div class="form-group">'
                                  +'<select name="" id="dia_desde" class="form-control dias">'
                                    +'<option value="">Seleccionar..</option>'
                                    +'<option value="1" selected>Lunes</option>'
                                    +'<option value="2">Martes</option>'
                                    +'<option value="3">Miércoles</option>'
                                    +'<option value="4">Jueves</option>'
                                    +'<option value="5">Viernes</option>'
                                    +'<option value="6">Sábado</option>'
                                    +'<option value="7">Domingo</option>'
                                  +'</select>'
                                +'</div>'
                                +'</div>'
                                
                                +'<div class="col-md-2">'
                                +'<div class="form-group">'
                                  +'<strong>Día hasta :</strong>'
                                +'</div>'
                                +'<div class="form-group">'
                                  +'<select name="" id="dia_hasta" class="form-control dias">'
                                    +'<option value="">Seleccionar..</option>'
                                    +'<option value="1">Lunes</option>'
                                    +'<option value="2">Martes</option>'
                                    +'<option value="3">Miércoles</option>'
                                    +'<option value="4">Jueves</option>'
                                    +'<option value="5">Viernes</option>'
                                    +'<option value="6">Sábado</option>'
                                    +'<option value="7" selected>Domingo</option>'
                                  +'</select>'
                                +'</div>'
                                +'</div>'

                              +'</div>'

                              +'<div class="col-md-12">'
                              	+'<span id="mensaje_stock"></span><hr>'
                                +'<div class="col-6 col-sm-6 col-lg-6 col-md-offset-3">'
                                  +'<div class="table-responsive">'
                                    +'<table class="table table-bordered">'
                                      +'<thead><tr>'
                                        +'<td>Lunes</td>'
                                        +'<td>Martes</td>'
                                        +'<td>Miércoles</td>'
                                        +'<td>Jueves</td>'
                                        +'<td>Viernes</td>'
                                        +'<td>Sábado</td>'
                                        +'<td>Domingo</td>'
                                      +'</tr></thead>'
                                      +'<tbody><tr>'
                                        +'<td align="center"><input type="checkbox" checked id="dia_1"  data-id="1" class="activar_dias"></td>'
                                        +'<td align="center"><input type="checkbox" checked id="dia_2" data-id="2" class="activar_dias"></td>'
                                        +'<td align="center"><input type="checkbox" checked  id="dia_3" data-id="3" class="activar_dias"></td>'
                                        +'<td align="center"><input type="checkbox" checked id="dia_4" data-id="4" class="activar_dias"></td>'
                                        +'<td align="center"><input type="checkbox" checked  id="dia_5" data-id="5" class="activar_dias"></td>'
                                        +'<td align="center"><input type="checkbox" checked id="dia_6" data-id="6" class="activar_dias"></td>'
                                        +'<td align="center"><input type="checkbox" checked id="dia_7" data-id="0" class="activar_dias"></td>'
                                      +'</tr></tbody>'
                                    +'</table>'
                                  +'</div>'
                                +'</div>'
                                +"<div class='col-6 col-sm-12 col-lg-6 col-md-offset-3 formulario'>"
					                  +'<div class="panel-group">'
					                    +'<div class="panel panel-warning">'
					                      +'<div class="panel-heading">'
					                        +'<h3 class="panel-title"><center>Tarifas y Categoría de habitación</center></h3>'
					                    +'</div>'
					                      +'<div class="panel-body">'
					                        
					                        +'<div class="form-group">'
					                          +'<select name="tarifa" id="tarifa" data-id="0" class="form-control select2 tarifas">'

					                            +'<option value="">Seleccionar..</option>'

					                           +'</select>'
					                          +'<i class="fa fa-close text-danger pull-right" id="error_tarifa" style="display:none;"></i>'
					                        +'</div>'


					                      
					                        +'<div class="form-group">'
					                          +'<select name = "habitacion" id="habitacion" class="form-control select2 habitaciones">'

					                            +'<option value="">Seleccionar..</option>'

					                          +'</select>'
					                          +'<i class="fa fa-close text-danger pull-right" id="error_habitacion_cts" style="display:none;"></i>'
					                        +'</div>'


					                        +'<div class="form-group">'

					                          +'<select name = "area" id="area" class="form-control area">'

					                            +'<option value="">Seleccionar área</option>'
					                            +'<option value="1">Receptivo</option>'
					                            +'<option value="2">Nacional</option>'

					                          +'</select>'

					                        +'</div>'

					                      +'</div>'
					                    +'</div>'
					                  +'</div>'
					                +'</div>'
                              +'</div>'
                          +'</div><button class="btn btn-success pull-right" id="enviar_form" data-editar="0" data-hotdet="0">Guardar Tarifa</button>';
           break;


  }


  return formulario;

}

  </script>


  </body>
</html>
