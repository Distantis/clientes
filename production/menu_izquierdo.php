<div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><span class="name_cliente"></span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="images/sin-foto.png" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Bienvenido,</span>
                <h2><span class="usuario">Usuario</span></h2>
              </div>
              <div class="clearfix"></div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-edit"></i> Cotizaciones <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="buscar_fecha_cot.php">Buscar Fecha Cot</a></li>
                      <li><a href="buscar_fecha_pax.php">Buscar Fecha Pax</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-folder-open"></i> RPT COT <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="rpt_cot_xmes.php">Realizadas por mes</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-folder-open"></i> RPT SISTEMA <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="dispo_toda.php">Disponibilidad (Global y Normal)</a></li>
                      <li><a href="operadores.php">Operadores</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-folder-open"></i> Maestros <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li>
                          <li><a><i class="fa fa-hotel"></i> Hotel <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                               <li><a href="buscar_hotel.php">Buscar hotel</a></li>
                              <li><a href="convenio_tarifario.php">Convenio Tarifario</a></li>
                              <li><a href="fecha_maxima_tarifa.php">Fecha Máxima de Tarifas</a></li>
                            </ul>
                          </li>


                          <li><a><i class="fa fa-users"></i> Usuarios <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                               <li><a href="nuevo_usuario.php">Nuevo Usuario</a></li>
                              <li><a href="buscar_usuario.php">Buscar Usuario</a></li>
                            </ul>
                          </li>

                          <li><a><i class="fa fa-male"></i> Clientes <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                               <li><a href="nuevo_cliente.php">Nuevo Cliente</a></li>
                              <li><a href="buscar_cliente.php">Buscar Cliente</a></li>
                            </ul>
                          </li>

                          <li><a><i class="fa fa-at"></i> Emails <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                               <li><a href="gestionar_mail.php">Gestionar</a></li>
                            </ul>
                          </li>


                      </li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-gears"></i> Otros Parámetros <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="markup_dinamico.php">Mark-UP Dinámico</a></li>
                      <li><a href="parametros_generales.php">Parámetros Generales</a></li>
                    </ul>
                  </li>
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <!--a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a-->
              <a data-toggle="tooltip" data-placement="top" title="Cerrar Sesión" href="login.html" class='pull-right'>
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>