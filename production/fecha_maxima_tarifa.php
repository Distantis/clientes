<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Nueva Empresa</title>

    <?php include("css.php");?>

    <link rel="stylesheet" type="text/css" href="css/select2.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        

        <?php include("menu_izquierdo.php");?>

        <!-- top navigation -->
        <?php include("menu_superior_derecho.php");?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3></h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Fecha Máxima Tarifa</h2>
                    <ul class="nav navbar-right panel_toolbox  pull-rigth">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <br><br>
                           <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group row">
                              <label for="area" class="col-md-4 col-sm-4 col-xs-12 col-form-label">Seleccionar Área</label>
                                <i class="fa fa-spinner fa-spin" id="spinner-area"></i>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <select id="area" name="area" class="form-control select2 pull-left" >
                                  </select>
                                </div>
                              </div>
                           </div>
                            
                          
                      </div>

                        <div class="row"></div>
                        
                         <div class="col-md-12 col-sm-12 col-xs-12 centered">
                          <br>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div  id="respuesta"></div>
                            </div>
                        
                         </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <input type="hidden" class="nombre_cliente">
        <!-- /page content -->

        <!-- footer content -->
        <?php include("footer.php");?>
        <!-- /footer content -->
      </div>
    </div>

    
    <?php include("jquery.php");?>

    <script type="text/javascript" src="js/select2.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/date-de.js"></script>
    
    <script type="text/javascript">
      


          $(document).on("ready", function(){

              $(".select2").attr("disabled","disabled");
              $(".select2").select2({
                  language: "es",
                  placeholder: "Seleccionar..",
                  allowClear: true
              });

             $('select').on('select2:open', function() {
              $('.select2-search--dropdown .select2-search__field').attr('placeholder', 'Buscar...');
            });



             $.post("sitio/Cliente.php",{tipo : 2},
                 function (data) {

                        try{
                         data = $.parseJSON(data);
                       }catch(err){
                         mostrar_notificacion("ERROR", "Esta ocurriendo un error en el sistema.", "error","stack-bottomleft");
                         return false;
                       }
                        var area = '<option></option>';
                        
                        $.each(data.area, function (i, datos) {
                            area += '<option  value="' + datos.id + '">' + datos.nombre + '</option>';
                        });

                         $("#area").html(area).removeAttr("disabled","disabled");
                         $("#spinner-area").removeClass("fa-spinner fa-spin");

             });



          });



          $(document).on("change","#area",function(){

              var valor = $(this).val();

              if(valor != ""){

                $("#respuesta").html("<center><i class='fa fa-spinner fa-spin fa-2x'></i></center>");
                  $.post("sitio/Data.php", {
                                  tipo: 6, area : valor
                              },
                      
                    function (data) {

                            data = $.parseJSON(data);

                            var tabla ='<table class="table table-bordered" id="tarifas_vencida" width="100%" ><thead>'
                                        +'<tr>'
                                            +'<th>Pk</th>'
                                            +'<th>Nombre Hotel</th>'
                                            +'<th>Ciudad</th>'
                                            +'<th>Area</th>'
                                            +'<th>Hotdet</th>'
                                            +'<th>Estado</th>'
                                            +'<th>Fecha max. tarifa</th>'
                                        +'</tr></thead><tbody>';


                            $.each(data.informacion, function(i,datos){

                              var color = "";
                              var estado = "VIGENTE";
                              if(datos.estado == 1){
                                color = "danger";
                                estado = "VENCIDA";
                              }

                              tabla += "<tr class='"+color+"'>"
                                      +"<td align='left'>"+datos.id_pk+"</td>"
                                      +"<td align='left'>"+datos.nombre_hotel+"</td>"
                                      +"<td align='left'>"+datos.ciudad+"</td>"
                                      +"<td align='left'>"+datos.area+"</td>"
                                      +"<td align='left'>"+datos.hotdet+"</td>"
                                      +"<td align='center'>"+estado+"</td>"
                                      +"<td align='left'>"+datos.maxima+"</td>"
                                        +"</tr>"  

                            });


                          tabla += "</tbody></table>";
                          $("#respuesta").html("<hr><br>"+tabla);

                        }

                    ).done(function(){
                      
                      var datatable = $("#tarifas_vencida").DataTable({
                            //"aaSorting": [[10, 'desc']],}
                             "lengthMenu": [[100, 150, 200, -1], [100, 150, 200, "Todos"]],
                              buttons: [ {
                                              extend:'excel',
                                              text:'',
                                              fieldBoundary: '"',
                                              className: 'fa fa-file-excel-o fa-2x text-success'
                                            },
                                            {
                                              extend:'pdf',
                                              text:'Descargar PDF',
                                              fieldBoundary: '"',
                                              className: 'btn-danger'
                                            }

                                   ],
                             "aaSorting": [[6, 'asc']],
                             "columnDefs": [ {
                                            "targets": 0,
                                            "orderable": false,
                                            },
                                            {
                                            "targets": 1,
                                            "orderable": false
                                            },
                                            {
                                            "targets": 2,
                                            "orderable": false
                                            },
                                            {
                                            "targets": 3,
                                            "orderable": false
                                            },
                                             {
                                            "targets": 4,
                                            "orderable": false
                                            },
                                            {
                                            "targets": 5,
                                            "orderable": false
                                            },

                                         ],
                            "aoColumns": [
                                        null,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null,
                                        {"sType": "date-euro" },
                            ],
                            //"scrollY":  "350px",
                            //"sScrollX": "100%",
                            //"bScrollCollapse": true,
                            "oLanguage": {
                                "sProcessing": "Procesando...",
                                "sLengthMenu": "Mostrar _MENU_ ",
                                "sZeroRecords": "No se han encontrado datos disponibles",
                                "sEmptyTable": "No se han encontrado datos disponibles",
                                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty": "Mostrando  del 0 al 0 de un total de 0 ",
                                "sInfoFiltered": "(filtrado de un total de _MAX_ datos)",
                                "sInfoPostFix": "",
                                "sSearch": "Buscar: ",
                                "sUrl": "",
                                "sInfoThousands": ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst": "Primero",
                                    "sLast": "Última",
                                    "sNext": "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            }
                      });

                      //datatable.buttons().container().prependTo(datatable.table().container());

                    });

              }else
                $("#respuesta").html("");


              
            });

    </script>

  </body>
</html>
